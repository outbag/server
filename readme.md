# Outbag Server

Did you know that you can host your own outbag instance? 

This repo contains the official outbag server.

## Setup
**\*Keep in mind that you need a valid SSL certificate for your domain**

### Using docker
[Find docker instructions in the docker repo](https://codeberg.org/outbag/containers)

### Manually
#### Requirements
- `git`
- `nodejs` & `npm`
- a MySQL database, we recommend `mariadb`

#### Setting up MariaDB

Set up the Maria Database.
Create a new database and a new user and grand the user all privileges to this database.
For more Instructions see: [Setup MariaDB](./docs/mysql.md)
   

#### Install
1. Clone this git repo
   ```bash
   git clone https://codeberg.org/outbag/server/ outbag-server
   cd outbag-server
   ```
2. Download & install the dependencies
   ```bash
   npm install
   ```
#### Setup
The Setup can either be done with environment variables or a config file.

See: [Configure Outbag](./docs/configuration.md)

Choosing environment variables: Set all needed environment variables from the `Configure Outbag` Page.

Choosing a config file: Run the Server with: `-c path/to/config.juml` arguments. If the config does not exist, a setup cli tool will be triggered. Enter all asked Information and double-check your config file. The setup tool can be triggered with `-s` at any time.

#### Port Forwarding & .well-known
The Outback server requires at least one forwarded TCP port. The default port is `443` or `7223`.

Each Server can be identified by a server tag. 
- Main-Tag: `example.com`
- Secondary-Tag: `example.com:port`

The app will find the server by fetching the path: `/.well-known/outbag/server` on `example.com` or `example.com:port`.
When the user enters no port and the first one is not available the second one will be tried with the port `7223`.
The `/.well-known/outbag/server` will be automatically hosted by the server. 

If your server should be accessible with the Main-Tag and is not running on port `443` or `7223` and on the path `/` you need to create/proxy the Well-known File at `example.com` or `example.com:7223`.    

### Hosting the UI
As of writing,
there is no official method of having the backend host the UI yet 
(we are working on it).

For the time being,
just follow the [manual guide](https://codeberg.org/outbag/app/).

## API
A documentation can be found [here](./docs/api.md).