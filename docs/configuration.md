# Configure Outbag
## config.juml
The Outback server can be configured with a configuration file using `-c` or `--config`. e.g.:
```bash
node . -c config.juml
```
If the configuration file does not exist, it is automatically generated and a setup cli tool is run. The setup tool can be called at any time with `-s` or `--setup`.

Available Settings:
- System:
    - **PORT**: That the Port on witch the server should start a server.
    - **PORTexposed**: The Port on witch Outbag will live.
    - **PATHexposed**: The Path on witch Outbag will live.
    - **URL**: The URL on witch outbag will live.
    - **CertLiveSec**: Every has an personal Certificate. After this time (in sec.) a newone will be generated. Recommended is one Month.
- ssl:
    - **enable**: Should the server start an ssl Engine. If set to false an proxy must do this job.
    - **privkey**: The path or link to an valid ssl privkey. Only needed when ssl is enabled.
    - **cert**: The path or link to an valid ssl cert. Only needed when ssl is enabled.
    - **chain**: The path or link to an valid ssl chain. Only needed when ssl is enabled.

- Database:
    - **host**: The hostname/url of the db server.
    - **port**: The port of the db server.
    - **user**: The user name that should be used for login in the db.
    - **password**: The password name that should be used for login in the db.
    - **database**: Th Database Out can/should exclusivly. 

- Settings:
    - **maxUsers**: Maxmimum number of accounts that can be created without OTA. Infinity is -1 or infinity.
    - **defaultMaxRooms**: The default maxmium amount of rooms a client can create. Infinity is -1 or infinity.
    - **defaultMaxRoomSize**: The default maxmium size a room can have. Infinity is -1 or infinity.
    - **defaultMaxUsersPerRoom**: The default maxmium amount of room members. Infinity is -1 or infinity.
    - **defaultViewable**: The default visibility for accounts. Viewable accounts can be listed by unauthorized users.

## Environment Variables
The Outbag server uses the following environment variables with these default values if no configuration is provided:

```
OUTBAG_PORT=7223
OUTBAG_EXPOSED_PORT=7223
OUTBAG_EXPOSED_PATH=/
OUTBAG_HOST=localhost
OUTBAG_CERT_LIVE_SEC=2592000

OUTBAG_SSL_ENABLED=false
OUTBAG_SSL_PRIVATE_KEY=privkey.pem
OUTBAG_SSL_CERT=cert.pem
OUTBAG_SSL_CHAIN=chain.pem

OUTBAG_MYSQL_HOST=localhost
OUTBAG_MYSQL_PORT=3306
OUTBAG_MYSQL_USER=admin
OUTBAG_MYSQL_PASSWORD=12346789
OUTBAG_MYSQL_DATABASE=outbag

OUTBAG_MAX_USERS=0
OUTBAG_DEFAULT_MAX_ROOMS=3
OUTBAG_DEFAULT_MAX_ROOMS_SIZE=10000
OUTBAG_DEFAULT_MAX_USERS_PER_ROOM=5
OUTBAG_DEFAULT_VIEWABLE=false
OUTBAG_DEL_ACCOUNT_KEEP_SEC=1728000
```

For an explanation, see the [config.juml](#configjuml) section.