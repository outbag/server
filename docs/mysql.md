# MySQL DB / MariaDB
Outbag uses a mysql db. We recommand mariaDB.
## Installation
Make sure you have a mysql db installed.

Otherwise install it on your System using your system's package manager and set it up.

### installation with apt
1. Install `mariadb` by executing the following command in your systems shell. e.g.:
   ```bash
   sudo apt install mariadb-server
   ```
2. Secure your installation
   ```bash
   sudo mysql_secure_installation
   ```

### installation with pacman
1. Install `mariadb` by executing the following command in your systems shell. e.g.:
   ```bash
   pacman -S mariadb
   ```
2. Secure your installation
   ```bash
   mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
   ```
3. Start and enable the mariadb installation:
    ```bash
    systemctl restart mariadb
    systemctl enable mariadb
    ```
4. Secure your installation
   ```bash
   sudo mysql_secure_installation
   ```

## Setting mariaDB up
1. Start the MySQL/MariaDB command line mode (you have to enter the befor given paswsord):
   ```bash
   sudo mysql -u root -p
   ```
2. Then a MariaDB [root]> prompt will appear.
   Create an user and a databse:
   ```sql
   CREATE USER 'outbagUser'@'localhost' IDENTIFIED BY 'aSecurePassword';
   CREATE DATABASE IF NOT EXISTS outbag CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
   GRANT ALL PRIVILEGES ON outbag.* TO 'outbagUser'@'localhost';
   FLUSH PRIVILEGES;
   ```
   Don't forget to replace the username, password and database name.
   Remember these values. You will need to enter this information later.
3. You can quit the prompt by entering:
   ```sql
   quit;
   ```