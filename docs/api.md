# Api
A client can send the server over [Post](#post) or [WebSocket](#websockrt) Api calls.
An api call allwas calls an Action/Act. 
Each Act has different [state](#state), [permission](#permissions), [data](#data) requirements to be used.

Some public data get be fetched using the [get Api](./api/get.md)

## Acts
All Acts:
- [login Acts](./api/acts_login.md)
- [client Acts](./api/acts_client.md)
- [admin Acts](./api/acts_admin.md)
- [rooms Acts](./api/acts_rooms.md)
- [roomContent Acts](./api/acts_roomContent.md)
- [subscribe Acts](./api/acts_subscribe.md)

Get requests: [get](./api/get.md)

### State
The Server keeps track of the state of the current state of the client connects.
When using [Post](#post) you have to use an Authentication methode in the header that directly puts you in the required state for the state that will be called.
Alternativly you can request a [requestTempToken](./api/actslogin.md#act-requesttemptoken) to reuse und change your state in multiple requests.

An overview can be found [here](./api/stats.md).

### Permissions
Some [client](./api/acts_client.md) and most [admin](./api/acts_admin.md) Acts require special Permissions to be excexuted.

An overview con be found [here](./api/permissions.md).

### Data
The Data send to an Act is a jsonObject. 
In the [Acts Pages](#acts) the key and type of each data-element can be found.

### Errors
All Acts may return an Error specified in the [Acts Pages](#acts) or a common error.

An overview of all errors can be found [here](./api/errors.md)

## Calls

### PassThrough
When interacting with a remote server, you can either call this server directly or through an act from your home server. Most [room](./api/acts_room.md) and [roomContent](./api/acts_roomContent.md) Acts allow to call the remote server through the home servers Act, hiding the clients ip-address. And you can use the same code for client and remote rooms.

### WebSocket
Open a WebSocket connection to the server. As long as the server will be reached the path does not matter.

Calling an act: Send the follwing json stringified to the server:
```ts
{
    act:  string,
    id:   number,
    data: any
}
```
- act: A string containing the name of the act.
- id: Some number set by the client, that will be returned. Should be used to keep different calls apart.
- data: a json Object, containing the payload for the called act.

Response:
```ts
{
    id: number,
    state: string,
    data: any
}
```
- id: same as send by the client
- state: `ok` or `error` or `server`
- data: 
 - if `ok`: The json data specified in the [Acts Pages](#acts).
 - if `error`: A string containing the [error code](./api/errors.md).
 - if `server`: This is not a response. This is an event send by the server.

### Post

Path: `https://[url]:[port]/[path]/api/[act]`

Headers:
- `Content-Type`: `application/json`
- `Authorization`: `Bearer [token]` can be used for authentication with a temp token.
- `Authorization`: `Digest name=[name] server=[server] accountKey=[password hased]` can be used for authentication.

Body:
```ts
{
    data: any
}
```
a json Object, containing the payload for the called act.

Response:
Code: 
- `200` - success
- `400` - error

Payload:
- if error: A string containing the [error code](./api/errors.md).
- if success: The json data specified in the [Acts Pages](#acts).