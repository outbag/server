## Admin Acts
All room Content related Acts, from the file [subscribe.ts](../../src/api/acts/subscribe.ts)

### Act: subscribeRoom

- **Description:** This act allows a user to subscribe to a specific room using WebSockets.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room to subscribe)
  - `server`: string (name of the server of the room)
- **Returned Data:**
  - No additional data returned.
- [**Errors:**](./errors.md)
  - `CONNECTION` if the client is not connected via WebSockets.
  - `NOT_IN_ROOM` if the user is not in the specified room.

---

### Act: unsubscribeRoom

- **Description:** This act allows a user to unsubscribe from a specific room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room to subscribe)
  - `server`: string (name of the server of the room)
- **Returned Data:**
  - No additional data returned.
- [**Errors:**](./errors.md)
  - `CONNECTION` if the client is not connected via WebSockets.
  - `NOT_IN_ROOM` if the user is not in the specified room.

---

### Act: unsubscribeAllRooms

- **Description:** This act allows a user to unsubscribe from all rooms.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - No additional data required.
- **Returned Data:**
  - No additional data returned.
- [**Errors:**](./errors.md)
  - `CONNECTION` if the client is not connected via websockets.