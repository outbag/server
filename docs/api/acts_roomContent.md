## Admin Acts
All room Content related Acts, from the file [roomContent.ts](../../src/api/acts/roomContent.ts)

### Act: getCategories

- **Description:** Retrieves a list of categories within a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:** An array of:
  - `id`: number (id of the category)
  - `title`: string (category title)
  - `weight`: number (category weight to order them ascending)
  - `color`: string (color of the category)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.

---

### Act: getCategory

- **Description:** Retrieves details of a specific category within a room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listCatID`: number (category ID)
- **Returned Data:**
  - `id`: number (id of the category)
  - `title`: string  (category title)
  - `weight`: number (category weight to order them ascending)
  - `color`: string (color of the category)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.
  - `CAT_NOT_EXISTS` if the specified category does not exist.

---

### Act: addCategory

- **Description:** Adds a new category to the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - [**Room:**](./permissions.md) `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `title`: string (category title, max length: 256)
  - `color`: string (category color, max length: 32)
- **Returned Data:**
  - `catID`: number (ID of the newly added category)
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `ROOM_DATA_LIMIT` if the room data limit is reached.
  - `ADD_CAT` if there's an issue adding the category.

---

### Act: changeCategory

- **Description:** Modifies details of an existing category in the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listCatID`: number (category ID)
  - `title`: string (category title, max length: 256)
  - `color`: string (category color, max length: 32)
- **Returned Data:** No specific returned data.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `CAT_NOT_EXISTS` if the specified category does not exist.

---

### Act: changeCategoriesOrder

- **Description:** Changes the order of categories within the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: String (server of the room)
  - `listCatIDs`: Array of Numbers, representing category IDs in the new order.
- **Returned Data:** No specific returned data.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `CAT_NOT_EXISTS` if one of the specified categories does not exist.

---

### Act: deleteCategory

- **Description:** Deletes a category from the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listCatID`: number (category ID)
- **Returned Data:** No specific returned data.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `CAT_NOT_EXISTS` if the specified category does not exist.

---

### Act: getProducts

- **Description:** Retrieves a list of products from a room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (the room identifier)
  - `server`: string (the server identifier)
- **Returned Data:** An array of:
  - `listProdID`: number (product ID)
  - `title`: string (product title)
  - `description`: string (product description)
  - `category`: number (product category ID, null if none)
  - `defUnit`: number (default unit)
  - `defValue`: string (default value)
  - `ean`: string (product EAN)
  - `parent`: number (parent product ID, null if none)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.

---

### Act: getProduct

- **Description:** Retrieves details of a specific product within a room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listProdID`: number (product ID)
- **Returned Data:**
  - `listProdID`: number (product ID)
  - `title`: string (product title)
  - `description`: string (product description)
  - `category`: number (category ID, null if none)
  - `defUnit`: number (default unit)
  - `defValue`: string (default value)
  - `ean`: string (product EAN)
  - `parent`: number (parent product ID, null if none)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.
  - `PROD_NOT_EXISTS` if the specified product does not exist.

---

### Act: addProduct

- **Description:** Adds a new product to the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `title`: string (product title, max length: 256)
  - `description`: string (product description, max length: 4096)
  - `listCatID`: number (category ID, null if none)
  - `defUnit`: number (default unit)
  - `defValue`: string (default value)
  - `ean`: string (product EAN, max length: 64)
  - `parent`: number (parent product ID, null if none)
- **Returned Data:**
  - `listProdID`: number (product ID)
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `ROOM_DATA_LIMIT` if the room data limit is reached.
  - `CAT_NOT_EXISTS` if the specified category does not exist (if provided).
  - `PROD_NOT_EXISTS` if the specified parent product does not exist (if provided).
  - `ADD_PROD` if there's an issue adding the product.

---

### Act: changeProduct

- **Description:** Allows the user to change product details, such as title, description, category, etc.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listProdID`: number (product ID)
  - `title`: string (product title, max length: 256 characters)
  - `description`: string (product description, max length: 4096 characters)
  - `listCatID`: number (category ID, null if none)
  - `defUnit`: number (default unit)
  - `defValue`: string (default value, max length: 256 characters)
  - `ean`: string (product EAN, max length: 64 characters)
  - `parent`: number (parent product ID, null if none)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin in the specified room.
  - `PROD_NOT_EXISTS` if the specified product does not exist.
  - `CAT_NOT_EXISTS` if the specified category does not exist (if provided).
  - `PROD_NOT_EXISTS` if the specified parent product does not exist

---

### Act: deleteProduct

- **Description:** Allows the user to delete a product from a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `VIEW_LIST_CAT_PROD`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listProdID`: number (product ID)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not a room admin.
  - `PROD_NOT_EXISTS` if the specified product does not exist in the room.

---

### Act: getItems


- **Description:** This act retrieves a list of items within a specified room. 
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:** An array of:
  - `listItemID`: number (item ID)
  - `state`: number (0 = added; 1 = in cart/bought)
  - `title`: string (item title)
  - `description`: string (product description)
  - `listCatID`: number (linked category ID, null if none)
  - `unit`: number (unit)
  - `value`: string (value)
  - `listProdID`: number (linked product ID, null if none)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not a member of the specified room.

---

### Act: getItem

- **Description:** Retrieves details of a specific item from a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listItemID`: number (item ID)
- **Returned Data:**
  - `listItemID`: number (item ID)
  - `state`: number (0 = added; 1 = in cart/bought)
  - `title`: string (item title)
  - `description`: string (product description)
  - `listCatID`: number (linked category ID, null if none)
  - `unit`: number (unit)
  - `value`: string (value)
  - `listProdID`: number (linked product ID, null if none)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.
  - `ITEM_NOT_EXISTS` if the specified item does not exist in the room.

---

### Act: addItem

- **Description:** Adds a new item to a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `ADD_ITEM`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `state`: number (0 = added; 1 = in cart/bought)
  - `title`: string (max length: 256)
  - `description`: string (max length: 4096)
  - `listCatID`: number (linked category ID, null if none)
  - `unit`: number (unit)
  - `value`: string (value, max length: 256)
  - `listProdID`: number (linked product ID, null if none)
- **Returned Data:**
  - `listItemID`: number (id of the added item) 
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `DATA` if the provided state is not valid (must be 0 or 1).
  - `ROOM_DATA_LIMIT` if the room's data storage is full.
  - `CAT_NOT_EXISTS` if the specified category does not exist in the room (if provided).
  - `PROD_NOT_EXISTS` if the specified linked product does not exist in the room (if provided).
  - `ADD_ITEM` if there is an error adding the item to the database.

---

### Act: changeItem

- **Description:** Allows the user to change details of a specific item in a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `ADD_ITEM`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listItemID`: number (id of the changed item) 
  - `state`: number (0 = added; 1 = in cart/bought)
  - `title`: string (max length: 256)
  - `description`: string (max length: 4096)
  - `listCatID`: number (linked category ID, null if none)
  - `unit`: number (unit)
  - `value`: string (value, max length: 256)
  - `listProdID`: number (linked product ID, null if none)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the user is not an admin in the specified room.
  - `DATA` if the provided state is not valid (must be 0 or 1).
  - `ITEM_NOT_EXISTS` if the specified item does not exist.
  - `CAT_NOT_EXISTS` if the specified category does not exist in the room.
  - `PROD_NOT_EXISTS` if the specified product does not exist in the room.

---

### Act: changeItemState

- **Description:** Allows the user to change the state of a specific item in a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listItemID`: number (id of the changed item) 
  - `state`: number (0 = added; 1 = in cart/bought)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the user is not in the specified room.
  - `DATA` if the provided state is not valid (must be 0 or 1).
  - `ITEM_NOT_EXISTS` if the specified item does not exist.

---

### Act: changeItemStates

- **Description:** Allows the user to change states of multiple items in a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `listItemIDs`: array of numbers (id of the changed items) 
  - `changedTimes`: array of numbers (in uts time sec)
  - `states`: array of numbers (0 = added; 1 = in cart/bought)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the user is not in the specified room.
  - `DATA` provided item ids are bad or array size are not equal.

---

### Act: deleteItem

- **Description:** Deletes a specified item from a room's list.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `REMOVE_ITEM`
- **Data:**
  - `room`: string (the name of the room)
  - `server`: string (identifier for the server)
  - `listItemID`: number (identifier of the item to be deleted)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN`: If the client does not have the required admin rights for the specified room.
  - `ITEM_NOT_EXISTS`: If the specified item does not exist in the room's list.

---

### Act: deleteItemByState

- **Description:** Allows the user to delete items with a specific state from a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `REMOVE_ITEM`
- **Data:**
  - `room`: string (the name of the room)
  - `server`: string (identifier for the server)
  - `state`: number (0 = added; 1 = in cart/bought)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN`: If the client does not have the required admin rights for the specified room.