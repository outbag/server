## Get Api
Some public data can be fetched via get.

Path: `https://[url]:[port]/[path]/api/[get-name]`

### Request: users
- **Description:** Retrieves a list of all viewable clients from this server.
- **Returned Data:** An array of client names.

### Request: isUser/\[user\]
- **Description:** Check if a user exists on this server
- **Returned Data:** "ok" or "error"

### Request: publicRooms
- **Description:** Retrieves a list of public rooms.
- **Returned Data:** An array of:
  - `name`: string (name of the room)
  - `server`: string (server of the room)
  - `title`: string (title of the room)
  - `description`: string (description of the room)
  - `icon`: string (name of the icon of the room)
  - `debug`: boolean (room server is in debug mode)

### Request: server/publicKey
- **Description:** Retrieve the public key of the server.
- **Returned Data:** 
  - `publicKey`: string (public rsa key, length: 4096, publicExponent: `[1, 0, 1]`, hash: `SHA-256`)
  - `expires`: number (uts in secounds)

### Request: server/state
- **Description:** Gets the state of the server. Retrives whether the server is in debug mode.
- **Returned Data:** 
  - `debug`: boolean