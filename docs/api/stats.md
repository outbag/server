## State
Each connection has a server internal state. An Act may or may not be called debending on the user state.

The States are:
- no: Only [login Acts](./acts_login.md) are available
- remoteP: First remote login step is completed.
- remote: Authenticated as remote client / as client from a different server.
- client: Authenticated as client from the current server.