## Admin Acts
All admin related Acts, from the file [admin.ts](../../src/api/acts/admin.ts)

### Act: serverStats

- **Description:** Fetches and provides various statistics about the server, including room and account counts, server uptime, hardware uptime, load average, memory usage, and operating system details.
- **State:** `STATE.client` (no state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and any of `VIEW_SERVER_STATS` required (requires at least one other admin permission).
- **Data:** No additional data required.
- **Returned Data:**
  - `currentRoomCount`: number (current number of rooms on the server).
  - `currentAccountCount`: number (current number of user accounts on the server).
  - `serverUptime`: number (server uptime in seconds).
  - `hardwareUptime`: number (hardware uptime in seconds).
  - `loadavg`: array (load average of the server; 1m, 5m, 15m).
  - `totalmem`: number (total system memory in bytes).
  - `freemem`: number (free system memory in bytes).
  - `os`: string (operating system type, release, and architecture).
- [**Errors:**](./errors.md) Only common errors.

---

### Act: getAccounts

- **Description:** Retrieves a list of user accounts with specified details.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `SHOW_USERS_AND_LISTS`
- **Data:** No additional data required.
- **Returned Data:** An array of:
  - `accID`: number (Account ID)
  - `rights`: number (User rights)
  - `name`: string (Account name)
  - `viewable`: boolean (Whether the account is viewable)
  - `deleted`: boolean (Whether the account is deleted)
  - `deletedTime`: number (Timestamp of deletion, 0 if not deleted)
  - `maxRooms`: number (Maximum allowed rooms)
  - `maxRoomSize`: number (Maximum room size)
  - `maxUsersPerRoom`: number (Maximum users per room)
- [**Errors:**](./errors.md) Only common errors.

---

### Act: recoverAccount

- **Description:** Recovers a deleted user account.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `EDIT_USERS`
- **Data:**
  - `accID`: number (Account ID)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ACCOUNT_NOT_EXISTS` if the specified account does not exist.

---

### Act: setPermissions

- **Description:** Sets or updates the permissions for a user account.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `EDIT_RIGHTS`
- **Data:**
  - `accID`: number (Account ID)
  - `rights`: number (New user rights)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ACCOUNT_NOT_EXISTS` if the specified account does not exist.

---

### Act: resetPassword

- **Description:** Resets the password for a user account.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `EDIT_USERS`
- **Data:**
  - `accID`: number (Account ID)
  - `accountKey`: string (New account key)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ACCOUNT_NOT_EXISTS` if the specified account does not exist.

---

### Act: setMaxValues

- **Description:** Sets or updates the maximum values for a user account.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `EDIT_USERS`
- **Data:**
  - `accID`: number (Account ID)
  - `maxRooms`: number (New maximum allowed rooms)
  - `maxRoomSize`: number (New maximum room size)
  - `maxUsersPerRoom`: number (New maximum users per room)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ACCOUNT_NOT_EXISTS` if the specified account does not exist.

---

### Act: getOTAs

- **Description:** Retrieves a list of active OTA tokens.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `MANAGE_OTA_TOKENS`
- **Data:** No additional data required.
- **Returned Data:** An array of:
  - `token`: string (OTA token)
  - `expires`: number (Timestamp when the token expires)
  - `usesLeft`: number (Remaining uses of the token)
- [**Errors:**](./errors.md) Only common errors.

---

### Act: addOTA

- **Description:** Adds or updates an OTA token.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `MANAGE_OTA_TOKENS`
- **Data:**
  - `token`: string (OTA token, max length 255)
  - `expires`: number (Timestamp when the token expires)
  - `usesLeft`: number (Number of uses left for the token)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md) Only common errors.

---

### Act: deleteOTA

- **Description:** Deletes an OTA token.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `MANAGE_OTA_TOKENS`
- **Data:**
  - `token`: string (OTA token)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md) Only common errors.
