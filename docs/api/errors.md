# Error Code Documentation
The server can send an error for various reasons. The errors are groupt into 3 groups. 
Common Errors can be thrown by any act. The other errors will be thrown by aa variety of acts, but will not be thrown by every act.

## Errors
### Common Errors
- **CLOSED** (`closed`): The server is closed and not accepting requests.
- **BRUTEFORCE** (`bruteforce`): The request was canceled due to suspected brute force activity. Retry the request later.
- **NOT_FOUND** (`notfound`): The specified action does not exist.
- **WRONG_STATE** (`wrongstate`): The action cannot be executed in the current state.
- **DATA** (`data`): The sent data does not match the expected data structure of the action.
- **RIGHT** (`right`): Your server-wide permissions do not allow you to perform this action.
- **SERVER** (`server`): An uncaught error occurred on the server.
- **RECURSION** (`recursion`): The action is not allowed due to suspected remote recursion. This error may only appear with misconfiguration.
- **REMOTE** (`remote`): Error occurred during a remote request, such as being unable to contact the remote server.
- **CONNECTION** (`connection`): The current connection type is incorrect.

### Client + Admin Errors
- **CLIENT_NOT_EXISTS** (`clientnotexists`): Your own account does not exist (client.ts acts).
- **ACCOUNT_NOT_EXISTS** (`accountnotexists`): The referred account does not exist (admin/client separation).
- **ACCOUNT_EXISTS** (`accountexists`): The referred account already exists.
- **ROOM_EXISTS** (`roomexists`): The new room already exists.
- **ROOM_NOT_EXISTS** (`roomnotexists`): The requested/referred room does not exist or does not exist for you.
- **MEMBER_NOT_EXISTS** (`membernotexists`): The referred member does not exist (always not you).
- **MEMBER_EXISTS** (`memberexists`): The referred member already exists.
- **DUPLICATE** (`duplicate`): You are already a member.
- **ROOM_LIMIT** (`roomlimit`): You have exceeded your room number limit.
- **ROOM_USER_LIMIT** (`roomuserlimit`): The room is full.
- **ROOM_DATA_LIMIT** (`roomdatalimit`): The room is full.
- **NOT_ROOM_ADMIN** (`notroomadmin`): You are not an admin of this room, if it exists
- **NOT_IN_ROOM** (`notinroom`): You are not in this room.
- **OWNER**  (`owner`): You are the owner, so you are not allowed to perform this action.
- **CAT_NOT_EXISTS** (`catnotexists`): The referred category does not exist.
- **PROD_NOT_EXISTS** (`prodnotexists`): The referred product does not exist.
- **ITEM_NOT_EXISTS** (`itemnotexists`): The referred item does not exist.
- **ADD_CAT** (`addcat`): Adding a category did not work.
- **ADD_PROD** (`addprod`): Adding a product did not work.
- **ADD_ITEM** (`additem`): Adding an item did not work.
- **CONFIG** (`config`): The server is full, and an OTA is needed to proceed.
- **OTA** (`ota`): The provided OTA is invalid.
- **AUTH** (`auth`): Provided authentication information is incorrect.
- **SIGNATURE** (`signature`): Cannot verify the provided signature.
- **TOKEN** (`token`): The authentication token is invalid.

### Server-to-Server Only Errors
- **SERVER_NOT_EXISTS** (`servernotexists`): Remote server login cannot proceed as the requesting server's information is missing.
- **SERVER_TOKEN** (`serverToken`): Wrong server token during server-to-server communication.