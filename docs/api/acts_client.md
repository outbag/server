## Admin Acts
All client related Acts, from the file [client.ts](../../src/api/acts/client.ts)


### Act: deleteAccount

- **Description:** This act allows the client to delete their account.
- **State:** `STATE.client` (state changes to `STATE.no` upon successful deletion)
- **Permissions:** No special permissions needed.
- **Data:** No additional data required.
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md) Only common errors.

---

### Act: createSignature

- **Description:** This act generates a signature for the client's public key (a Certificate for the client).
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API` and `PROVIDE_CERT`
- **Data:**
  - `publicKey`: string (the public key for which the signature is generated)
- **Returned Data:**
  - `sign`: string (the generated signature)
- [**Errors:**](./errors.md) Only common errors.

---

### Act: getMyAccount

- **Description:** Retrieves information about the client's account.
- **State:** `STATE.client` (No state change)
- **Permissions:** No special permissions needed.
- **Data:** No additional data required.
- **Returned Data:**
  - `rights`: number (perissions the client has)
  - `name`: string (clients user name)
  - `viewable`: boolean (client is publical viewable)
  - `maxRooms`: number
  - `maxRoomSize`: number
  - `maxUsersPerRoom`: number
- [**Errors:**](./errors.md)
  - `CLIENT_NOT_EXISTS` if the client does not exist.

---

### Act: changePassword

- **Description:** Allows the client to change their account password.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API`
- **Data:**
  - `accountKey`: string (the new account password)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `CLIENT_NOT_EXISTS` if the client does not exist.

---

### Act: changeViewable

- **Description:** Allows the client to change the viewable status of their account.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API`
- **Data:**
  - `viewable`: boolean (the new viewable status)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `CLIENT_NOT_EXISTS` if the client does not exist.

---

### Act: createRoom

- **Description:** Allows the client to create a new room.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API`
- **Data:**
  - `room`: string (name of the new room, max length: 100)
  - `title`: string (title of the new room, max length: 255)
  - `description`: string (description of the new room, max length: 255)
  - `visibility`: number (0: not visible, 1: visible on the server, 2: publicly visible)
  - `icon`: string (icon of the new room, max length: 255)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ROOM_LIMIT` if the client exceeds the room creation limit.
  - `ROOM_EXISTS` if the room with the same name already exists.

---

### Act: deleteRoom

- **Description:** Allows the client to delete a room they own.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API`
- **Data:** 
  - `room`: string (name of the room to be deleted)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ROOM_NOT_EXISTS` if the specified room does not exist or is not owned by the client.

---

### Act: listPublicRooms

- **Description:** Retrieves a list of public rooms.
- **State:** `STATE.client` (No state change)
- [**Permissions:**](./permissions.md) `CAN_USE_API`
- **Data:** No additional data required.
- **Returned Data:** An array of:
  - `name`: string (name of the room)
  - `server`: string (server of the room)
  - `visibility`: number (0: not visible, 1: visible on the server, 2: publicly visible)
  - `title`: string (title of the room)
  - `description`: string (description of the room)
  - `icon`: string (name of the icon of the room)
  - `debug`: bool (room server is in debug mode)
- [**Errors:**](./errors.md) Only common errors.