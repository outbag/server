## Rooms Acts
All room related Acts, from the file [rooms.ts](../../src/api/acts/rooms.ts)


### Act: listRooms

- **Description:** Retrieves a list of available rooms, including local rooms and those from remote servers.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:** No additional data required.
- **Returned Data:** An array of:
  - `name`: string (name of the room)
  - `server`: string (server of the room)
  - `owner`: string (name of the owner of the room)
  - `rights`: number (none admin rights in the room)
  - `visibility`: number (0: not visible, 1: visible on the server, 2: publicly visible)
  - `title`: string (title of the room)
  - `description`: string (description of the room)
  - `icon`: string (name of the icon of the room)
  - `debug`: boolean (room server is in debug mode)
  - `confirmed`: boolean (confirmation status, if false this is an invitation)
- [**Errors:**](./errors.md) Only common errors.


---

### Act: getRoomInfo

- **Description:** Retrieves information about a specific chat room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:**
  - `name`: string (name of the chat room)
  - `server`: string (server identifier)
  - `owner`: string (owner of the chat room)
  - `rights`: number (none admin rights in this room)
  - `visibility`: number (0: not visible, 1: visible on the server, 2: publicly visible)
  - `title`: string (title of the room)
  - `description`: string (description of the room)
  - `icon`: string (name of the icon of the room)
  - `isAdmin`: boolean (indicates if the client is an admin in the room)
  - `isOwner`: boolean (indicates if the client is the owner of the room)
- [**Errors:**](./errors.md)
  - `ROOM_NOT_EXISTS` if the specified chat room does not exist.

---

### Act: getRoomMembers

- **Description:** Retrieves information about members of a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:** An array of:
  - `name`: string (name of the room)
  - `server`: string (server of the room)
  - `admin`: boolean (Indicates if the room member is an admin)
  - `confirmed`: boolean (Indicates if the room member accepted the invitation)
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not a member of the specified room.


---

### Act: joinRoom

- **Description:** This act allows a client to join a specific room using a provided token for authorization.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `token`: string (OTA token for authorization)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ROOM_NOT_EXISTS` if the specified room does not exist.
  - `ROOM_USER_LIMIT` if the room has reached its user limit.
  - `OTA` if there is an issue with the provided OTA token.

---

### Act: joinPublicRoom

- **Description:** This act allows a client to join a public room without requiring an OTA token.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `ROOM_NOT_EXISTS` if the specified room does not exist.
  - `ROOM_USER_LIMIT` if the room has reached its user limit.

---

### Act: getRoomOTAs

- **Description:** Retrieves information about OTAs for a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `OTA`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:** An array of:
  - `token`: string (OTA token)
  - `name`: string (OTA name)
  - `expires`: number (timestamp when the OTA expires, or -1 if never)
  - `usesLeft`: number (remaining uses of the OTA, or -1 if infinite)
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin for the specified room.

---

### Act: addRoomOTA

- **Description:** Adds or updates an OTA for a specified room. The token is the primary key.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `OTA`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `token`: string (OTA token, max length 256)
  - `name`: string (OTA name, max length 256)
  - `expires`: number (timestamp when the OTA expires, or -1 if never)
  - `usesLeft`: number (remaining uses of the OTA, or -1 if infinite)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin for the specified room.

---

### Act: deleteRoomOTA

- **Description:** Deletes an OTA for a specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `OTA`
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
  - `token`: string (OTA token)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin for the specified room.

---

### Act: inviteUser

- **Description:** Invites a user to a specified room with the given details.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `OTA`
- **Data:**
  - `room`: string (name of the room, max length 100)
  - `roomServer`: string (server of the room)
  - `name`: string (name of the user to be invited)
  - `server`: string (name of the room of the user)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `ACCOUNT_NOT_EXISTS` if the invited user does not exists does not exist.
  - `MEMBER_EXISTS` if the user is already a member of the room.

---

### Act: confirmRoom

- **Description:** Confirms a room on the local or remote server.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room, max length 100)
  - `server`: string (server of the room)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.
  - `MEMBER_NOT_EXISTS` if the member does not exist in the room.

---

### Act: kickMember

- **Description:** Kicks a member from the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `MANAGE_MEMBERS`
- **Data:**
  - `room`: string (name of the room)
  - `roomServer`: string (server of the room)
  - `name`: string (name of the user to be kicked)
  - `server`: string (name of the room of the user)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `MEMBER_NOT_EXISTS` if the member to be kicked does not exist in the room.

---

### Act: setAdminStatus

- **Description:** Sets or revokes admin status for a member in the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `CHANGE_ADMIN`
- **Data:**
  - `room`: string (name of the room)
  - `roomServer`: string (server of the room)
  - `name`: string (name of the user)
  - `server`: string (name of the room of the user)
  - `admin`: boolean (true for admin, false to revoke admin status)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `MEMBER_NOT_EXISTS` if the member does not exist in the room.

---

### Act: leaveRoom

- **Description:** Makes the client leave the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
- **Data:**
  - `room`: string (name of the room)
  - `server`: string (server of the room)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_IN_ROOM` if the client is not in the specified room.
  - `OWNER` if the client is the owner of the room.

---

### Act: setVisibility

- **Description:** Sets the visibility of the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `OTA`
- **Data:**
  - `room`: string (room name)
  - `server`: string (server tag for the room)
  - `visibility`: number (0: not visible, 1: visible on the server, 2: publicly visible)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.
  - `DATA` if the provided visibility data is invalid.

---

### Act: setRoomRight

- **Description:** Sets the rights for none admin users of the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `CHANGE_ADMIN`
- **Data:**
  - `room`: string (room name)
  - `server`: string (server tag for the room)
  - `rights`: number (rights for none admins as per permissions.ts)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.

---

### Act: changeRoomMeta

- **Description:** Changes the metadata of the specified room.
- **State:** `STATE.client` or `STATE.remote`
- **Permissions:** No special permissions needed.
  - **Room:** `CHANGE_META`
- **Data:**
  - `room`: string (room name)
  - `server`: string (server tag for the room)
  - `title`: string (up to 255 characters)
  - `description`: string (up to 255 characters)
  - `icon`: string (up to 255 characters)
- **Returned Data:** No specific data returned.
- [**Errors:**](./errors.md)
  - `NOT_ROOM_ADMIN` if the client is not an admin of the specified room.