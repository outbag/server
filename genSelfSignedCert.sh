mkdir -p openssl
openssl req -nodes -new -x509 -keyout openssl/server.key -out openssl/server.cert -subj '/CN=localhost'
touch openssl/server.chain