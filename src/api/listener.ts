import { wsClient } from "./ws.js"



interface RoomListener {
    client: wsClient;
    id: any,
    room: string;
    server: string;
}

let roomListeners: { [key: number]: RoomListener[] } = {};

export const sendRoomListeners = async (roomID: number, type: string | number, action: number, ids: number[]) => {
    for (const listener of roomListeners[roomID] ?? []) {
        listener.client.sendFromServer(listener.id, {
            room: listener.room,
            server: listener.server,
            type,
            action,
            ids,
        });
    }
};

export const addRoomListener = (client: wsClient, id: any, roomID: number, room: string, server: string) => {
    if (!Array.isArray(roomListeners[roomID])) roomListeners[roomID] = [];
    const existingListener = roomListeners[roomID].find(listener => listener.client === client);
    if (!existingListener) roomListeners[roomID].push({ client, id, room, server });

};

export const removeRoomListener = (client: wsClient, roomID: number | null = null) => {
    let keys = roomID == null ? Object.keys(roomListeners) as any : [roomID];
    for (const key of keys) {
        roomListeners[key] = roomListeners[key].filter(listener => listener.client !== client);
        if (roomListeners[key].length === 0) delete roomListeners[key];
    }
};
