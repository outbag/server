import { addShutdownTask } from "nman";
import { sign } from "../sys/crypto.js";
import { outbagServer } from "../server/outbagURL.js";
import { uts } from "../sys/tools.js";
import { getSettings, SETTINGS } from "../sys/settings.js";
import { selfTag } from "../sys/selfTag.js";
import { act_error } from "../server/errors.js";

let remoteTempTokens: { [key: string]: { lastReq: number, token: string } } = {};

async function sendPost(server: outbagServer, header: object, act: string, data: any) {
    let fetchResp = await fetch(server.httpsURL + "api/" + act, {
        method: "POST",
        headers: Object.assign({
            "Content-Type": "application/json"
        }, header),
        body: JSON.stringify({ data })
    });

    return {
        state: fetchResp.status == 200 ? "ok" : "error",
        data: (await fetchResp.json()).data
    };
}

export const getServerToken = async (server: outbagServer, force = false) => {
    if (
        !force
        && remoteTempTokens[server.tag] != null
        && remoteTempTokens[server.tag].lastReq + 60 * 60 * 1 > uts()
    ) return remoteTempTokens[server.tag].token;
    try {
        let resp = await sendPost(server, {}, "remoteServer1", {
            server: selfTag.tag
        });
        if (resp.state != "ok") return false;
        let token = resp.data.token;
        let challenge = resp.data.challenge;
        resp = await sendPost(
            server,
            { "authorization": `Bearer ${token}` },
            "remoteServer2",
            { sign: await sign(challenge, await getSettings(SETTINGS.privateKey)) }
        );
        if (resp.state != "ok") return false;
        remoteTempTokens[server.tag] = { lastReq: uts(), token };
        return token;
    } catch (error) {
        return false;
    }

}

export const fetchRemoteAs = async (server: outbagServer, name: string, act: string, data: any) => {
    try {
        let token = await getServerToken(server);
        if (token === false) throw new Error("remote");
        let resp = await sendPost(
            server,
            { "authorization": `outbagServer ${token} as=${name}` },
            act,
            data
        );
        if (resp.state != "error" || resp.data != act_error.SERVER_TOKEN) return resp;
        token = await getServerToken(server, true);
        if (token === false) throw new Error("remote");
        resp = await sendPost(
            server,
            { "authorization": `outbagServer ${token} as=${name}` },
            act,
            data
        );
        if (resp.state == "error" && resp.data == act_error.SERVER_TOKEN) return {
            state: "error",
            data: act_error.REMOTE
        }
        return resp;
    } catch (error) {
        return {
            state: "error",
            data: act_error.REMOTE
        }
    }
};

export const fetchRemoteAsServer = async (server: outbagServer, act: string, data: any) => {
    try {
        let token = await getServerToken(server);
        if (token === false) throw new Error("remote");
        let resp = await sendPost(
            server,
            { "authorization": `Bearer ${token}` },
            act,
            data
        );
        if (resp.state != "error" || (resp.data != act_error.TOKEN && resp.data != act_error.SERVER_TOKEN)) return resp;
        token = await getServerToken(server, true);
        if (token === false) throw new Error("remote");
        resp = await sendPost(
            server,
            { "authorization": `Bearer ${token}` },
            act,
            data
        );
        if (resp.state == "error" && (resp.data == act_error.TOKEN || resp.data == act_error.SERVER_TOKEN)) return {
            state: "error",
            data: act_error.REMOTE
        }
        return resp;
    } catch (error) {
        return {
            state: "error",
            data: act_error.REMOTE
        }
    }
};

let cancleClear = setInterval(() => {
    let keys = Object.keys(remoteTempTokens);
    for (let i = 0; i < keys.length; i++) {
        const c = remoteTempTokens[keys[i]];
        if (c.lastReq + 60 * 60 * 1 < uts()) {
            delete remoteTempTokens[keys[i]];
        }
    }
}, 1000);

addShutdownTask(() => void clearInterval(cancleClear));