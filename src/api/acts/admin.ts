import { alias, and, count, eq, geq, insert, le, or, remove, select, update } from "dblang";
import { PERMISSIONS } from "../../server/permissions.js";
import { sha256 } from "../../sys/crypto.js";
import { accounts, db, rooms, signupOTA } from "../../sys/db.js";
import { get64, uts } from "../../sys/tools.js";
import { Act, Client, STATE } from "../user.js";
import { act_error } from "../../server/errors.js";
import * as os from "os";

export const serverStats: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!client.checkAnyRight(PERMISSIONS.VIEW_SERVER_STATS))
            return void aws("error", act_error.RIGHT);
        let currentRoomCount = alias(select([count(rooms.roomID)], rooms), "currentRoomCount") as any;
        let currentAccountCount = alias(select([count(accounts.accID)], accounts), "currentAccountCount") as any;
        let req = await select([currentRoomCount, currentAccountCount], null)
            .query(db);
        aws("ok", {
            currentRoomCount: Number(req[0][currentRoomCount]),
            currentAccountCount: Number(req[0][currentAccountCount]),
            serverUptime: uts() - Math.floor(process.uptime()),
            hardwareUptime: uts() - Math.floor(os.uptime()),
            loadavg: os.loadavg(),
            totalmem: os.totalmem(),
            freemem: os.freemem(),
            os: `${os.type()} ${os.release().split("-")[0]} ${os.arch()}`,
        });
    }
};

export const getAccounts: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.SHOW_USERS,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let query = await select([
            accounts.accID,
            accounts.rights,
            accounts.name,
            accounts.viewable,
            accounts.deleted,
            accounts.deletedTime,
            accounts.maxRooms,
            accounts.maxRoomSize,
            accounts.maxUsersPerRoom
        ], accounts)
            .query(db);
        var out = query.map(d => {
            let accID = d[accounts.accID];
            let rights = d[accounts.rights];
            let name = d[accounts.name];
            let viewable = d[accounts.viewable] ? true : false;
            let deleted = d[accounts.deleted] ? true : false;
            let deletedTime = Number(d[accounts.deletedTime]);
            let maxRooms = d[accounts.maxRooms];
            let maxRoomSize = d[accounts.maxRoomSize];
            let maxUsersPerRoom = d[accounts.maxUsersPerRoom];
            if (accID != null && rights != null && name != null && viewable != null && deleted != null && maxRooms != null && maxRoomSize != null && maxUsersPerRoom != null && deletedTime != null) {
                return { accID, rights, name, viewable, deleted, deletedTime, maxRooms, maxRoomSize, maxUsersPerRoom };
            }
            return null;
        });
        aws("ok", out.filter(d => d != null));
    }
};

export const recoverAccount: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.EDIT_USERS,
    data: {
        accID: "number"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let req = await update(accounts)
            .set(accounts.deleted, false)
            .set(accounts.deletedTime, 0)
            .where(eq(accounts.accID, data.accID))
            .query(db);
        if (req.affectedRows > 0) aws("ok", "");
        else aws("error", act_error.ACCOUNT_NOT_EXISTS)
    }
}

export const setPermissions: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.EDIT_RIGHTS,
    data: {
        accID: "number",
        rights: "number"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let query = await update(accounts)
            .set(accounts.rights, data.rights)
            .where(eq(accounts.accID, data.accID))
            .query(db);
        if (query.affectedRows > 0) {
            aws("ok", "");
        } else {
            client.suspect();
            aws("error", act_error.ACCOUNT_NOT_EXISTS);
        }
    }
};

export const resetPassword: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.EDIT_USERS,
    data: {
        accID: "number",
        accountKey: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        var salt = get64(16);
        let req = await update(accounts)
            .set(accounts.accountKey, sha256(salt + data.accountKey))
            .set(accounts.accountKeySalt, salt)
            .where(eq(accounts.accID, data.accID))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            client.suspect();
            aws("error", act_error.ACCOUNT_NOT_EXISTS);
        }
    }
};

export const setMaxValues: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.EDIT_USERS,
    data: {
        accID: "number",
        maxRooms: "number",
        maxRoomSize: "number",
        maxUsersPerRoom: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let req = await update(accounts)
            .set(accounts.maxRooms, data.maxRooms)
            .set(accounts.maxRoomSize, data.maxRoomSize)
            .set(accounts.maxUsersPerRoom, data.maxUsersPerRoom)
            .where(eq(accounts.accID, data.accID))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            client.suspect();
            aws("error", act_error.ACCOUNT_NOT_EXISTS);
        }
    }
};

export const getOTAs: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.MANAGE_OTA_TOKENS,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        await remove(signupOTA)
            .where(or(
                eq(signupOTA.usesLeft, 0),
                and(
                    le(signupOTA.expires, uts()),
                    geq(signupOTA.expires, 0)
                )
            ))
            .query(db);
        let req = await select([
            signupOTA.token,
            signupOTA.name,
            signupOTA.expires,
            signupOTA.usesLeft,
        ], signupOTA)
            .query(db);
        aws("ok", req.map(d => ({
            token: d[signupOTA.token],
            name: d[signupOTA.name],
            expires: Number(d[signupOTA.expires]),
            usesLeft: d[signupOTA.usesLeft]
        })));
    }
};

export const addOTA: Act = { // or change it, primary key is the token
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.MANAGE_OTA_TOKENS,
    data: {
        token: "string-256",
        name: "string-256",
        expires: "number", //uts in sec.
        usesLeft: "number"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        try {
            let resp = await insert(signupOTA.token, signupOTA.name, signupOTA.expires, signupOTA.usesLeft)
                .add(data.token, data.name, data.expires, data.usesLeft)
                .query(db);
            if (resp.affectedRows == 0) throw new Error("insertion fail");
        } catch (error) {
            await update(signupOTA)
                .set(signupOTA.name, data.name)
                .set(signupOTA.expires, data.expires)
                .set(signupOTA.usesLeft, data.usesLeft)
                .where(eq(signupOTA.token, data.token))
                .query(db);
        }
        aws("ok", "");
    }
};

export const deleteOTA: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.MANAGE_OTA_TOKENS,
    data: {
        token: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        await remove(signupOTA)
            .where(eq(signupOTA.token, data.token))
            .query(db);
        aws("ok", "");
    }
};