import { and, eq, ge, insert, leq, remove, select, update } from "dblang";
import { PERMISSIONS } from "../../server/permissions.js";
import { sha256, sign } from "../../sys/crypto.js";
import { accounts, db, roomMembers, rooms } from "../../sys/db.js";
import { selfTag } from "../../sys/selfTag.js";
import { getSettings, SETTINGS } from "../../sys/settings.js";
import { get64, uts } from "../../sys/tools.js";
import { Act, Client, STATE } from "../user.js";
import { canCreateRoom } from "../helper.js"
import { act_error } from "../../server/errors.js";


export const deleteAccount: Act = {
    state: STATE.client,
    right: 0,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        client.state = STATE.no;
        await update(accounts)
            .set(accounts.deleted, 1)
            .set(accounts.deletedTime, uts())
            .where(eq(accounts.accID, client.accID))
            .query(db);
        aws("ok", "");
    }
};

export const createSignature: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API | PERMISSIONS.PROVIDE_CERT,
    data: {
        publicKey: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        const server = selfTag;
        const tag = `${client.name}@${server.host}:${server.port}-${data.publicKey}`;
        var signature = await sign(tag, await getSettings(SETTINGS.privateKey));
        aws("ok", { sign: signature });
    }
};

export const getMyAccount: Act = {
    state: STATE.client,
    right: 0,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let query = await select([
            accounts.rights,
            accounts.name,
            accounts.viewable,
            accounts.maxRooms,
            accounts.maxRoomSize,
            accounts.maxUsersPerRoom,
        ], accounts)
            .where(eq(accounts.accID, client.accID))
            .query(db);
        if (query.length > 0) {
            let rights = query[0][accounts.rights];
            let name = query[0][accounts.name];
            let viewable = query[0][accounts.viewable] ? true : false;
            let maxRooms = query[0][accounts.maxRooms];
            let maxRoomSize = query[0][accounts.maxRoomSize];
            let maxUsersPerRoom = query[0][accounts.maxUsersPerRoom];
            if (rights != null && name != null && viewable != null && maxRooms != null && maxRoomSize != null && maxUsersPerRoom != null) {
                aws("ok", { rights, name, viewable, maxRooms, maxRoomSize, maxUsersPerRoom });
                return;
            }
        }
        client.suspect();
        aws("error", act_error.CLIENT_NOT_EXISTS);
    }
};

export const changePassword: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API,
    data: {
        accountKey: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        var salt = get64(16);
        let req = await update(accounts)
            .set(accounts.accountKey, sha256(salt + data.accountKey))
            .set(accounts.accountKeySalt, salt)
            .where(eq(accounts.accID, client.accID))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            client.suspect();
            aws("error", act_error.CLIENT_NOT_EXISTS);
        }
    }
};

export const changeViewable: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API,
    data: {
        viewable: "boolean"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let req = await update(accounts)
            .set(accounts.viewable, data.viewable)
            .where(eq(accounts.accID, client.accID))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            client.suspect();
            aws("error", act_error.CLIENT_NOT_EXISTS);
        }
    }
};

export const createRoom: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API,
    data: {
        room: "name-100",
        title: "string-255",
        description: "string-255",
        visibility: "number",
        icon: "string-255"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!await canCreateRoom(client.accID))
            return void aws("error", act_error.ROOM_LIMIT);
        try {
            let req = await insert(
                rooms.name,
                rooms.owner,
                rooms.visibility,
                rooms.title,
                rooms.description,
                rooms.icon
            ).add(
                data.room,
                client.accID,
                data.visibility,
                data.title,
                data.description,
                data.icon
            ).query(db);
            if (req.affectedRows > 0) {
                await insert(roomMembers.roomID, roomMembers.name, roomMembers.server, roomMembers.admin, roomMembers.confirmed)
                    .add(req.insertId, client.name, "local", true, true)
                    .query(db);
                aws("ok", "");
            } else {
                client.suspect();
                aws("error", act_error.ROOM_EXISTS);
            }
        } catch (error) {
            client.suspect();
            aws("error", act_error.ROOM_EXISTS);
        }
    }
};

export const deleteRoom: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API,
    data: {
        room: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let req = await remove(rooms)
            .where(and(
                eq(rooms.name, data.room),
                eq(rooms.owner, client.accID)
            )).query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            aws("error", act_error.ROOM_NOT_EXISTS);
        }
    }
};

export const listPublicRooms: Act = {
    state: STATE.client,
    right: PERMISSIONS.CAN_USE_API,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let req = await select([
            rooms.name,
            rooms.visibility,
            rooms.title,
            rooms.description,
            rooms.icon
        ], rooms)
            .where(ge(rooms.visibility, 0))
            .query(db);
        let out = req.map(d => {
            let name = d[rooms.name];
            let visibility = d[rooms.visibility];
            let title = d[rooms.title];
            let description = d[rooms.description];
            let icon = d[rooms.icon];
            if (name != null && visibility != null && title != null && description != null && icon != null) {
                return { name, server: selfTag.tag, visibility, title, description, icon, debug: global.debug };
            }
            return null;
        });
        aws("ok", out.filter(d => d != null));
    }
};
