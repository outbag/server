import { and, coalesce, eq, insert, le, leftJoinOn, leftJoinUsing, max, order, plus, remove, select, update } from "dblang";
import { checkSelfTag } from "../../server/outbagURL.js";
import { Act, Client, STATE } from "../user.js";
import { db, listCategories, listItems, listProducts } from "../../sys/db.js";
import { isCategoryInRoom, isItemInRoom, isProductInRoom, isRoomDataFull } from "../helper.js"
import { ROOM_RIGHTS } from "../../server/permissions.js";
import { act_error } from "../../server/errors.js";
import { uts } from "../../sys/tools.js";
import { sendRoomListeners } from "../listener.js";
import { LISTENER_ACTION, LISTENER_TYPE } from "../../server/listener.js";

export const getCategories: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getCategories", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            listCategories.listCatID,
            listCategories.title,
            listCategories.weight,
            listCategories.color
        ], listCategories)
            .where(eq(listCategories.roomID, roomID))
            .orderBY(listCategories.weight, order.ASC)
            .query(db);
        let out = req.map(d => {
            let id = d[listCategories.listCatID];
            let title = d[listCategories.title];
            let weight = d[listCategories.weight];
            let color = d[listCategories.color];
            return { id, title, weight, color };
        }).filter(d => d != null);
        out.sort((a, b) => a.weight - b.weight);
        aws("ok", out);
    }
};

export const getCategory: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listCatID: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getCategory", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            listCategories.listCatID,
            listCategories.title,
            listCategories.weight,
            listCategories.color
        ], listCategories)
            .where(and(
                eq(listCategories.roomID, roomID),
                eq(listCategories.listCatID, data.listCatID)
            ))
            .query(db);
        if (req.length > 0) {
            aws("ok", {
                id: req[0][listCategories.listCatID],
                title: req[0][listCategories.title],
                weight: req[0][listCategories.weight],
                color: req[0][listCategories.color]
            });
        } else {
            aws("error", act_error.CAT_NOT_EXISTS);
        }
    }
};

export const addCategory: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        title: "string-256",
        color: "string-32",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "addCategory", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        if (await isRoomDataFull(roomID)) return void aws("error", act_error.ROOM_DATA_LIMIT);
        let req = await insert(
            listCategories.roomID,
            listCategories.title,
            listCategories.weight,
            listCategories.color
        ).setSelect(
            select([
                roomID,
                data.title,
                plus(coalesce(max(listCategories.weight), 0), 1),
                data.color
            ], listCategories)
                .where(eq(listCategories.roomID, roomID))
                .limit(1)
        ).query(db);
        if (req.affectedRows > 0) {
            aws("ok", {
                catID: Number(req.insertId)
            });
            sendRoomListeners(roomID, LISTENER_TYPE.CATEGORIES, LISTENER_ACTION.ADD, [Number(req.insertId)]);
        } else aws("error", act_error.ADD_CAT);
    }
};

export const changeCategory: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listCatID: "number",
        title: "string-256",
        color: "string-32"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeCategory", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        let req = await update(listCategories)
            .set(listCategories.title, data.title)
            .set(listCategories.color, data.color)
            .where(and(
                eq(listCategories.roomID, roomID),
                eq(listCategories.listCatID, data.listCatID)
            ))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.CATEGORIES, LISTENER_ACTION.CHANGE, [data.listCatID]);
        } else aws("error", act_error.CAT_NOT_EXISTS);
    }
};

export const changeCategoriesOrder: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listCatIDs: "array-number"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeCategoriesOrder", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        let affacted = 0;
        for (let i = 0; i < data.listCatIDs.length; i++) {
            const listCatID = data.listCatIDs[i];
            let req = await update(listCategories)
                .set(listCategories.weight, i)
                .where(and(
                    eq(listCategories.roomID, roomID),
                    eq(listCategories.listCatID, listCatID)
                ))
                .query(db);
            affacted += req.affectedRows;
        }
        if (affacted > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.CATEGORIES, LISTENER_ACTION.CHANGE, data.listCatIDs);
        } else aws("error", act_error.CAT_NOT_EXISTS);
    }
};

export const deleteCategory: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listCatID: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "deleteCategory", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        let req = await remove(listCategories)
            .where(and(
                eq(listCategories.roomID, roomID),
                eq(listCategories.listCatID, data.listCatID)
            ))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.CATEGORIES, LISTENER_ACTION.DELETE, [data.listCatID]);
        } else aws("error", act_error.CAT_NOT_EXISTS);
    }
};

export const getProducts: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getProducts", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            listProducts.listProdID,
            listProducts.title,
            listProducts.description,
            listProducts.category,
            listProducts.defUnit,
            listProducts.defValue,
            listProducts.ean,
            listProducts.parent
        ], listProducts)
            .where(eq(listProducts.roomID, roomID))
            .query(db);
        let out = req.map(d => {
            let listProdID = d[listProducts.listProdID];
            let title = d[listProducts.title];
            let description = d[listProducts.description];
            let category = d[listProducts.category];
            let defUnit = d[listProducts.defUnit];
            let defValue = d[listProducts.defValue];
            let ean = d[listProducts.ean];
            let parent = d[listProducts.parent];
            return { listProdID, title, description, category, defUnit, defValue, ean, parent };
        });
        aws("ok", out.filter(d => d != null));
    }
};

export const getProduct: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listProdID: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getProducts", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            listProducts.listProdID,
            listProducts.title,
            listProducts.description,
            listProducts.category,
            listProducts.defUnit,
            listProducts.defValue,
            listProducts.ean,
            listProducts.parent
        ], listProducts)
            .where(and(
                eq(listProducts.listProdID, data.listProdID),
                eq(listProducts.roomID, roomID),
            ))
            .query(db);
        if (req.length > 0) {
            let listProdID = req[0][listProducts.listProdID];
            let title = req[0][listProducts.title];
            let description = req[0][listProducts.description];
            let category = req[0][listProducts.category];
            let defUnit = req[0][listProducts.defUnit];
            let defValue = req[0][listProducts.defValue];
            let ean = req[0][listProducts.ean];
            let parent = req[0][listProducts.parent];
            aws("ok", { listProdID, title, description, category, defUnit, defValue, ean, parent });
        } else {
            aws("error", act_error.PROD_NOT_EXISTS);
        }
    }
};

export const addProduct: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        title: "string-256",
        description: "string-4096",
        listCatID: "nullpointer",
        defUnit: "number",
        defValue: "string-256",
        ean: "string-64",
        parent: "nullpointer" //null = no parent
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "addProduct", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        if (await isRoomDataFull(roomID)) return void aws("error", act_error.ROOM_DATA_LIMIT);
        if (data.listCatID != null && !isCategoryInRoom(roomID, data.listCatID)) return void aws("error", act_error.CAT_NOT_EXISTS);
        if (data.parent != null && !isProductInRoom(roomID, data.parent)) return void aws("error", act_error.PROD_NOT_EXISTS);
        let req = await insert(
            listProducts.roomID,
            listProducts.title,
            listProducts.description,
            listProducts.category,
            listProducts.defUnit,
            listProducts.defValue,
            listProducts.ean,
            listProducts.parent,
        ).add(
            roomID,
            data.title,
            data.description,
            data.listCatID != null ? data.listCatID : null,
            data.defUnit,
            data.defValue,
            data.ean,
            data.parent != null ? data.parent : null,
        ).query(db);
        if (req.affectedRows > 0) {
            aws("ok", {
                listProdID: Number(req.insertId)
            });
            sendRoomListeners(roomID, LISTENER_TYPE.PRODUCTS, LISTENER_ACTION.ADD, [Number(req.insertId)]);
        } else aws("error", act_error.ADD_PROD);
    }
};

export const changeProduct: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listProdID: "number",
        title: "string-256",
        description: "string-4096",
        listCatID: "nullpointer",
        defUnit: "number",
        defValue: "string-256",
        ean: "string-64",
        parent: "nullpointer" //null = no parent
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeProduct", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        if (!isProductInRoom(roomID, data.listProdID)) return void aws("error", act_error.PROD_NOT_EXISTS);
        if (data.listCatID != null && !isCategoryInRoom(roomID, data.listCatID)) return void aws("error", act_error.CAT_NOT_EXISTS);
        if (data.parent != null && !isProductInRoom(roomID, data.parent)) return void aws("error", act_error.PROD_NOT_EXISTS);

        let req = await update(listProducts)
            .set(listProducts.title, data.title)
            .set(listProducts.description, data.description)
            .set(listProducts.category, data.listCatID != null ? data.listCatID : null)
            .set(listProducts.defUnit, data.defUnit)
            .set(listProducts.defValue, data.defValue)
            .set(listProducts.ean, data.ean)
            .set(listProducts.parent, data.parent != null ? data.parent : null)
            .where(and(
                eq(listProducts.listProdID, data.listProdID),
                eq(listProducts.roomID, roomID),
            ))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.PRODUCTS, LISTENER_ACTION.CHANGE, [data.listProdID]);
        } else aws("error", act_error.PROD_NOT_EXISTS);
    }
};

export const deleteProduct: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listProdID: "number"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "deleteProduct", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.VIEW_LIST_CAT_PROD);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        if (!isProductInRoom(roomID, data.listProdID)) return void aws("error", act_error.PROD_NOT_EXISTS);
        let req = await remove(listProducts)
            .where(and(
                eq(listProducts.listProdID, data.listProdID),
                eq(listProducts.roomID, roomID),
            )).query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.PRODUCTS, LISTENER_ACTION.DELETE, [data.listProdID]);
        } else aws("error", act_error.PROD_NOT_EXISTS);
    }
};

export const getItems: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getItems", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            listItems.listItemID,
            listItems.state,
            listItems.title,
            listItems.description,
            listItems.category,
            listItems.unit,
            listItems.value,
            listItems.link,
        ], leftJoinOn(listItems, listCategories, eq(listItems.category, listCategories.listCatID)))
            .where(eq(listItems.roomID, roomID))
            .orderBY(listItems.state, order.ASC)
            .addOrderBy(listCategories.weight, order.ASC)
            .query(db);
        let out = req.map(d => {
            let listItemID = Number(d[listItems.listItemID]);
            let state = d[listItems.state];
            let title = d[listItems.title];
            let description = d[listItems.description];
            let listCatID = d[listItems.category];
            let unit = d[listItems.unit];
            let value = d[listItems.value];
            let listProdID = d[listItems.link];
            return {
                listItemID,
                state, // 0 = added; 1 = in cart/bought
                title,
                description,
                listCatID,
                unit,
                value,
                listProdID
            };
        });
        aws("ok", out.filter(d => d != null));
    }
};

export const getItem: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listItemID: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getItems", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            listItems.listItemID,
            listItems.state,
            listItems.title,
            listItems.description,
            listItems.category,
            listItems.unit,
            listItems.value,
            listItems.link,
        ], listItems)
            .where(and(
                eq(listItems.listItemID, data.listItemID),
                eq(listItems.roomID, roomID)
            ))
            .query(db);
        if (req.length > 0) {
            let listItemID = Number(req[0][listItems.listItemID]);
            let state = req[0][listItems.state];
            let title = req[0][listItems.title];
            let description = req[0][listItems.description];
            let listCatID = req[0][listItems.category];
            let unit = req[0][listItems.unit];
            let value = req[0][listItems.value];
            let listProdID = req[0][listItems.link];
            aws("ok", {
                listItemID,
                state, // 0 = added; 1 = in cart/bought
                title,
                description,
                listCatID,
                unit,
                value,
                listProdID
            });
        } else {
            aws("error", act_error.ITEM_NOT_EXISTS);
        }
    }
};

export const addItem: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        state: "number",
        title: "string-256",
        description: "string-4096",
        listCatID: "nullpointer", //null = no parent
        unit: "number",
        value: "string-256",
        listProdID: "nullpointer" //null = no parent
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "addItem", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.ADD_ITEM);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        if (data.state != 1 && data.state != 0) return void aws("error", act_error.DATA);
        if (await isRoomDataFull(roomID)) return void aws("error", act_error.ROOM_DATA_LIMIT);
        if (data.listCatID != null && !isCategoryInRoom(roomID, data.listCatID)) return void aws("error", act_error.CAT_NOT_EXISTS);
        if (data.listProdID != null && !isProductInRoom(roomID, data.listProdID)) return void aws("error", act_error.PROD_NOT_EXISTS);
        let req = await insert(
            listItems.roomID,
            listItems.state,
            listItems.title,
            listItems.description,
            listItems.category,
            listItems.unit,
            listItems.value,
            listItems.link,
        ).add(
            roomID,
            data.state,
            data.title,
            data.description,
            data.listCatID != null ? data.listCatID : null,
            data.unit,
            data.value,
            data.listProdID != null ? data.listProdID : null,
        ).query(db);
        if (req.affectedRows > 0) {
            aws("ok", {
                listItemID: Number(req.insertId)
            });
            sendRoomListeners(roomID, LISTENER_TYPE.ITEMS, LISTENER_ACTION.ADD, [Number(req.insertId)]);
        } else aws("error", act_error.ADD_ITEM);
    }
};

export const changeItem: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listItemID: "number",
        state: "number", // 0 = added; 1 = in cart/bought
        title: "string-256",
        description: "string-4096",
        listCatID: "nullpointer", //null = no parent
        unit: "number",
        value: "string-256",
        listProdID: "nullpointer" //null = no parent
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeItem", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.ADD_ITEM);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        if (data.state != 1 && data.state != 0) return void aws("error", act_error.DATA);
        if (!isItemInRoom(roomID, data.listItemID)) return void aws("error", act_error.ITEM_NOT_EXISTS);
        if (data.listCatID != null && !isCategoryInRoom(roomID, data.listCatID)) return void aws("error", act_error.CAT_NOT_EXISTS);
        if (data.listProdID != null && !isProductInRoom(roomID, data.listProdID)) return void aws("error", act_error.PROD_NOT_EXISTS);
        let req = await update(listItems)
            .set(listItems.state, data.state)
            .set(listItems.title, data.title)
            .set(listItems.description, data.description)
            .set(listItems.category, data.listCatID != null ? data.listCatID : null)
            .set(listItems.unit, data.unit)
            .set(listItems.value, data.value)
            .set(listItems.link, data.listProdID != null ? data.listProdID : null)
            .where(and(
                eq(listItems.listItemID, data.listItemID),
                eq(listItems.roomID, roomID)
            ))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.ITEMS, LISTENER_ACTION.CHANGE, [data.listItemID]);
        } else aws("error", act_error.ITEM_NOT_EXISTS);
    }
};

export const changeItemState: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listItemID: "number",
        state: "number", // 0 = added; 1 = in cart/bought
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeItemState", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        if (data.state != 1 && data.state != 0) return void aws("error", act_error.DATA);
        if (!isItemInRoom(roomID, data.listItemID)) return void aws("error", act_error.ITEM_NOT_EXISTS);
        let req = await update(listItems)
            .set(listItems.state, data.state)
            .set(listItems.lastStateTime, uts())
            .where(and(
                eq(listItems.listItemID, data.listItemID),
                eq(listItems.roomID, roomID)
            ))
            .query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.ITEMS, LISTENER_ACTION.CHANGE, [data.listItemID]);
        } else aws("error", act_error.ITEM_NOT_EXISTS);
    }
};

export const changeItemStates: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listItemIDs: "array-number",
        changedTimes: "array-number", //in uts time sec
        states: "array-number", // 0 = added; 1 = in cart/bought
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeItemStates", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        if (
            data.listItemIDs.length != data.changedTimes.length
            || data.changedTimes.length != data.states.length
        ) return void aws("error", act_error.DATA);
        for (let i = 0; i < data.listItemIDs.length; i++) {
            let id = data.listItemIDs[i];
            if (typeof id != "number" || id < 0)
                return void aws("error", act_error.DATA);
        }
        let out: number[] = [];
        for (let i = 0; i < data.listItemIDs.length; i++) {
            let id = data.listItemIDs[i];
            let time = data.changedTimes[i];
            let state = data.states[i] == 0 ? 0 : 1;
            let req = await update(listItems)
                .set(listItems.state, state)
                .set(listItems.lastStateTime, uts())
                .where(and(
                    eq(listItems.listItemID, id),
                    eq(listItems.roomID, roomID),
                    le(listItems.lastStateTime, time)
                ))
                .query(db);
            if (req.affectedRows > 0) out.push(id);
        }
        aws("ok", "");
        sendRoomListeners(roomID, LISTENER_TYPE.ITEMS, LISTENER_ACTION.CHANGE, out);
    }
};

export const deleteItem: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        listItemID: "number"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "deleteItem", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.REMOVE_ITEM);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        let req = await remove(listItems)
            .where(and(
                eq(listItems.listItemID, data.listItemID),
                eq(listItems.roomID, roomID)
            )).query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
            sendRoomListeners(roomID, LISTENER_TYPE.ITEMS, LISTENER_ACTION.DELETE, [data.listItemID]);
        } else aws("error", act_error.ITEM_NOT_EXISTS);
    }
};
export const deleteItemByState: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        state: "number", // 0 = added; 1 = in cart/bought
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "deleteItemByState", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.REMOVE_ITEM);
        if (roomID == -1) {
            aws("error", act_error.NOT_ROOM_ADMIN);
            return;
        }
        let req = await remove(listItems)
            .where(and(
                eq(listItems.state, data.state),
                eq(listItems.roomID, roomID)
            )).query(db);
        aws("ok", "");
        sendRoomListeners(roomID, LISTENER_TYPE.ITEMS, LISTENER_ACTION.DELETE, []);
    }
}