import { act_error } from "../../server/errors.js";
import { checkSelfTag } from "../../server/outbagURL.js";
import { Act, Client, STATE } from "../user.js";
import { wsClient } from "../ws.js";

export const subscribeRoom: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "name",
        server: "string",
        eventID: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!(client.connectionClient instanceof wsClient)) {
            return void aws("error", act_error.CONNECTION);
        }
        if (!checkSelfTag(data.server)) {
            throw new Error("Remote not yet implemented.");
            return;
        }
        const roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        client.connectionClient.listenRoom(roomID, data.eventID, data.room, data.server);
        aws("ok", "");
    }
};

export const unsubscribeRoom: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "name",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!(client.connectionClient instanceof wsClient)) {
            return void aws("error", act_error.CONNECTION);
        }
        if (!checkSelfTag(data.server)) {
            throw new Error("Remote not yet implemented.");
            return;
        }
        const roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        client.connectionClient.unlistenRoom(roomID);
        aws("ok", "");
    }
};

export const unsubscribeAllRooms: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!(client.connectionClient instanceof wsClient)) {
            return void aws("error", act_error.CONNECTION);
        }
        // TODO: When implemented close remote listeners
        client.connectionClient.unlistenAllRooms();
        aws("ok", "");
    }
};