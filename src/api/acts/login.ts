import { select, count, alias, insert, remove, le, update, minus, eq, and, or, geq } from "dblang";
import { checkSelfTag, outbagServer, outbagURLfromTag } from "../../server/outbagURL.js";
import { PERMISSIONS } from "../../server/permissions.js";
import { getRemote } from "../../server/serverCerts.js";
import { oConf } from "../../sys/config.js";
import { sha256, verify } from "../../sys/crypto.js";
import { accounts, db, signupOTA as signupOTATable } from "../../sys/db.js";
import { selfTag } from "../../sys/selfTag.js";
import { get64, uts } from "../../sys/tools.js";
import { addTempToken, postClient } from "../post.js";
import { Act, Client, STATE } from "../user.js";
import { act_error } from "../../server/errors.js";

export const requestTempToken: Act = {
    state: STATE.no | STATE.remote | STATE.remoteP | STATE.client,
    right: 0,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void): Promise<void> => {
        let c = new postClient(client.ip, client);
        let token = addTempToken(c);
        aws("ok", {
            token
        });
    }
};

export const signup: Act = {
    state: STATE.no,
    right: 0,
    data: {
        name: "name-100",
        server: "string",
        accountKey: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            aws("error", act_error.DATA);
            return;
        }
        let countAlias = alias(count(accounts.accID), "countAlias") as any;
        let query = await select([countAlias], accounts)
            .query(db);
        let maxUsers = oConf.get("Settings", "maxUsers");
        let userNum = query[0][countAlias];
        if (maxUsers > -1 && userNum > 0 && userNum >= maxUsers) {
            aws("error", act_error.CONFIG);
            return;
        }
        let salt = get64(16);
        try {
            let req = await insert(accounts.name, accounts.rights, accounts.accountKey, accounts.accountKeySalt)
                .add(data.name, userNum == 0 ? PERMISSIONS.ALL : PERMISSIONS.DEFAULT, sha256(salt + data.accountKey), salt)
                .query(db);
            if (req.affectedRows > 0) {
                let accID = Number(req.insertId);
                if (!isNaN(accID)) {
                    aws("ok", "");
                    client.state = STATE.client;
                    client.accID = accID;
                    client.name = data.name;
                    client.server = new outbagServer(data.server, selfTag.host, selfTag.path, selfTag.port);
                }
            } else {
                client.suspect();
                aws("error", act_error.ACCOUNT_EXISTS);
            }
        } catch (error) {
            client.suspect();
            aws("error", act_error.ACCOUNT_EXISTS);
        }


    }
};

export const signupOTA: Act = {
    state: STATE.no,
    right: 0,
    data: {
        name: "string-100",
        server: "string",
        accountKey: "string",
        OTA: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            aws("error", act_error.DATA);
            return;
        }
        // TODO: make transaktion when posible
        await remove(signupOTATable)
            .where(or(
                eq(signupOTATable.usesLeft, 0),
                and(
                    le(signupOTATable.expires, uts()),
                    geq(signupOTATable.expires, 0)
                )
            ))
            .query(db);
        let query = await update(signupOTATable)
            .set(signupOTATable.usesLeft, minus(signupOTATable.usesLeft, 1))
            .query(db);
            await remove(signupOTATable)
            .where(or(
                eq(signupOTATable.usesLeft, 0),
                and(
                    le(signupOTATable.expires, uts()),
                    geq(signupOTATable.expires, 0)
                )
            ))
            .query(db);
        if (query.affectedRows == 0) {
            return void aws("error", act_error.OTA);
        }
        let salt = get64(16);
        let req = await insert(accounts.name, accounts.rights, accounts.accountKey, accounts.accountKeySalt)
            .add(data.name, PERMISSIONS.DEFAULT, sha256(salt + data.accountKey), salt)
            .query(db);
        if (req.affectedRows > 0) {
            let accID = Number(req.insertId);
            if (!isNaN(accID)) {
                aws("ok", "");
                client.state = STATE.client;
                client.accID = accID;
                client.name = data.name;
                client.server = new outbagServer(data.server, selfTag.host, selfTag.path, selfTag.port);
            }
        } else {
            client.suspect();
            aws("error", act_error.ACCOUNT_EXISTS);
        }
    }
};

export const signin: Act = {
    state: STATE.no,
    right: 0,
    data: {
        name: "string",
        server: "string",
        accountKey: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            aws("error", act_error.DATA);
            return;
        }
        let query = await select([accounts.accID, accounts.accountKey, accounts.accountKeySalt], accounts)
            .where(and(
                eq(accounts.name, data.name),
                eq(accounts.deleted, 0)
            ))
            .query(db);
        if (query.length == 0 || query[0][accounts.accountKey] != sha256((query[0][accounts.accountKeySalt] ?? '') + data.accountKey)) {
            client.suspect();
            aws("error", act_error.AUTH);
            return;
        }
        var accID = query[0][accounts.accID];
        if (!isNaN(accID)) {
            aws("ok", "");
            client.state = STATE.client;
            client.accID = accID;
            client.name = data.name;
            client.server = new outbagServer(data.server, selfTag.host, selfTag.path, selfTag.port);
        }
    }
};

export const remote1: Act = {
    state: STATE.no,
    right: 0,
    data: {
        name: "string",
        server: "string",
        publicKey: "string",
        sign: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        try {
            let server = await outbagURLfromTag(data.server);
            var cert = await getRemote(server);
            var tagAcert = `${data.name}@${server.host}:${server.port}-${data.publicKey}`;
            if (!(await verify(tagAcert, data.sign, cert))) {
                client.suspect();
                aws("error", act_error.SIGNATURE);
                return;
            }

            client.name = data.name;
            client.server = server;
            client.challenge = get64(64);
            client.state = STATE.remoteP;
            client.remoteKey = data.publicKey;
            aws("ok", { challenge: client.challenge });
        } catch (e) {
            client.suspect();
            aws("error", act_error.SIGNATURE);
        }
    }
};

export const remote2: Act = {
    state: STATE.remoteP,
    right: 0,
    data: {
        sign: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (await verify(client.challenge, data.sign, client.remoteKey)) {
            aws("ok", "");
            client.state = STATE.remote;
        } else {
            client.suspect();
            aws("error", act_error.SIGNATURE);
        }
    }
};

export const remoteServer1: Act = {
    state: STATE.no,
    right: 0,
    data: {
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        try {
            let server = await outbagURLfromTag(data.server);
            client.remoteKey = await getRemote(server);
            client.server = server;
            client.state = STATE.serverP;
            let c = new postClient(client.ip, client);
            let token = addTempToken(c);
            let challenge = get64(64);
            client.challenge = challenge;
            aws("ok", {
                token,
                challenge
            });
        } catch (e) {
            client.suspect();
            aws("error", act_error.SERVER_NOT_EXISTS);
        }
    }
};

export const remoteServer2: Act = {
    state: STATE.serverP,
    right: 0,
    data: {
        sign: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (await verify(client.challenge, data.sign, client.remoteKey)) {
            aws("ok", "");
            client.state = STATE.server;
        } else {
            client.suspect();
            aws("error", act_error.SERVER_TOKEN);
        }
    }
}; 