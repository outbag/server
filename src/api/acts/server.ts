import { eq, insert, select } from "dblang";
import { checkSelfTag } from "../../server/outbagURL.js";
import { accounts, db, remoteRooms } from "../../sys/db.js";
import { Act, Client, STATE } from "../user.js";
import { act_error } from "../../server/errors.js";

export const invite: Act = {
    state: STATE.server,
    right: 0,
    data: {
        room: "name-100",
        //roomServer: "string", 
        name: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) return void aws("error", act_error.ACCOUNT_NOT_EXISTS);
        let req = await select([accounts.accID], accounts)
            .where(eq(accounts.name, data.name))
            .query(db);
        if (req.length == 0) {
            client.suspect();
            aws("error", act_error.ACCOUNT_NOT_EXISTS);
            return;
        }
        let query = await insert(remoteRooms.accID, remoteRooms.server, remoteRooms.room, remoteRooms.confirmed)
            .add(req[0][accounts.accID], client.server.tag, data.room, false)
            .query(db);
        aws("ok", "");
    }
};