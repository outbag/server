import { alias, and, eq, exists, ge, geq, innerJoinOn, innerJoinUsing, insert, le, minus, naturalJoin, not, or, remove, select, update } from "dblang";
import { checkSelfTag, outbagServer, outbagURLfromTag } from "../../server/outbagURL.js";
import { ROOM_RIGHTS } from "../../server/permissions.js";
import { accounts, db, remoteRooms, roomMembers, roomOTAs, rooms } from "../../sys/db.js";
import { selfTag } from "../../sys/selfTag.js";
import { uts } from "../../sys/tools.js";
import { isRoomFull } from "../helper.js";
import { fetchRemoteAsServer } from "../server.js";
import { Act, Client, STATE } from "../user.js";
import { act_error } from "../../server/errors.js";
import { sendRoomListeners } from "../listener.js";
import { LISTENER_ACTION, LISTENER_TYPE } from "../../server/listener.js";

export const listRooms: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {},
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        let ownerAlias = alias(
            select([accounts.name], accounts)
                .where(eq(accounts.accID, rooms.owner)),
            "owner") as any;
        let req = await select([
            rooms.name,
            ownerAlias,
            rooms.rights,
            rooms.visibility,
            rooms.title,
            rooms.description,
            rooms.icon,
            roomMembers.confirmed
        ], innerJoinUsing(rooms, roomMembers, rooms.roomID, roomMembers.roomID))
            .where(and(
                eq(roomMembers.name, client.name),
                eq(roomMembers.server, client.state == STATE.client ? "local" : client.server.tag)
            ))
            .query(db);
        let out = req.map(d => {
            let name = d[rooms.name];
            let owner = d[ownerAlias];
            let rights = d[rooms.rights];
            let visibility = d[rooms.visibility];
            let title = d[rooms.title];
            let description = d[rooms.description];
            let icon = d[rooms.icon];
            let confirmed = d[roomMembers.confirmed] ? true : false;
            let server = selfTag.tag;
            if (name != null && owner != null && rights != null && visibility != null && title != null && description != null && icon != null && confirmed != null) {
                return { name, server, owner, rights, visibility, title, description, icon, debug: global.debug, confirmed };
            }
            //console.log(name, server, owner, rights, visibility, title, description, icon, global.debug, confirmed)
            return null;
        });
        if (client.state == STATE.client) {
            let query = await select([
                remoteRooms.server,
                remoteRooms.room,
                remoteRooms.confirmed
            ], remoteRooms)
                .where(eq(remoteRooms.accID, client.accID))
                .query(db);
            let serverList = [...new Set(query.map(t => t[remoteRooms.server]))];

            let toAddRooms: ([number, string, string])[] = [];

            for (let i = 0; i < serverList.length; i++) {
                const server = serverList[i];

                let serverRooms = Object.fromEntries(query
                    .filter(d => d[remoteRooms.server] == server)
                    .map(d => [d[remoteRooms.room] + "@" + d[remoteRooms.server], {
                        server: d[remoteRooms.server],
                        room: d[remoteRooms.room],
                        confirmed: d[remoteRooms.confirmed] ? true : false
                    }])
                );

                let resp = await client.pass(server, "listRooms", {});
                if (resp.state == "ok" && Array.isArray(resp.data)) {
                    for (let j = 0; j < resp.data.length; j++) {
                        const rRooms = resp.data[j];
                        try {
                            let { name, owner, rights, visibility, title, description, icon, debug } = rRooms;
                            if (name != null && owner != null && rights != null && visibility != null && title != null && description != null && icon != null && debug != null) {
                                let sRoom = serverRooms[name + "@" + server];
                                if (sRoom == null) toAddRooms.push([client.accID, name, server]);

                                out.push({
                                    name, owner, rights, visibility, title, description, icon, server, debug, confirmed: sRoom?.confirmed ?? false
                                });
                                delete serverRooms[name + "@" + server];
                            }
                        } catch (error) { }
                    }
                    for (const k in serverRooms) {
                        const unfoundRoom = serverRooms[k];
                        await remove(remoteRooms)
                            .where(and(
                                eq(remoteRooms.accID, client.accID),
                                eq(remoteRooms.server, unfoundRoom.server),
                                eq(remoteRooms.room, unfoundRoom.room)
                            )).query(db);
                    }
                } else {
                    //may remove unfound rooms
                }
            }
            try {
                await insert(remoteRooms.accID, remoteRooms.room, remoteRooms.server)
                    .addValues(...toAddRooms)
                    .query(db);
            } catch (error) { }
        }
        aws("ok", out.filter(d => d != null));
    }
};

export const getRoomInfo: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "name",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getRoomInfo", data);
            aws(resp.state, resp.data);
            return;
        }
        let ownerAlias = alias(
            select([accounts.name], accounts)
                .where(eq(accounts.accID, rooms.owner)),
            "owner") as any;
        let req = await select([
            rooms.name,
            ownerAlias,
            rooms.rights,
            rooms.visibility,
            rooms.title,
            rooms.description,
            rooms.icon,
            roomMembers.admin
        ], innerJoinUsing(rooms, roomMembers, rooms.roomID, roomMembers.roomID))
            .where(and(
                eq(roomMembers.name, client.name),
                eq(roomMembers.server, client.state == STATE.client ? "local" : client.server.tag),
                eq(rooms.name, data.room)
            ))
            .query(db);
        if (req.length == 0) return void aws("error", act_error.ROOM_NOT_EXISTS);
        aws("ok", {
            name: req[0][rooms.name],
            server: selfTag.tag,
            owner: req[0][ownerAlias],
            rights: req[0][rooms.rights],
            visibility: req[0][rooms.visibility],
            title: req[0][rooms.title],
            description: req[0][rooms.description],
            icon: req[0][rooms.icon],

            isAdmin: req[0][roomMembers.admin] ? true : false,
            isOwner: client.state == STATE.client && req[0][ownerAlias] == client.name,
        })

    }
};

export const getRoomMembers: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "name",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getRoomMembers", data);
            aws(resp.state, resp.data);
            return;
        }

        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) {
            aws("error", act_error.NOT_IN_ROOM);
            return;
        }
        let req = await select([
            roomMembers.name,
            roomMembers.server,
            roomMembers.admin,
            roomMembers.confirmed,
        ], roomMembers)
            .where(eq(roomMembers.roomID, roomID))
            .query(db);
        let out = req.map(d => {
            let name = d[roomMembers.name];
            let server = d[roomMembers.server];
            let admin = d[roomMembers.admin] ? true : false;
            let confirmed = d[roomMembers.confirmed] ? true : false;
            server = server == "local" ? selfTag.tag : server;
            if (name != null && server != null && admin != null && confirmed != null) {
                return { name, server, admin, confirmed };
            }
            return null;
        });
        aws("ok", out.filter(d => d != null));
    }
};

export const joinRoom: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        token: "string" //OTA
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "joinRoom", data);
            if (resp.state == "ok") {
                try {
                    await insert(remoteRooms.accID, remoteRooms.server, remoteRooms.room, remoteRooms.confirmed)
                        .add(client.accID, data.server, data.room, true)
                        .query(db);
                } catch (error) { }
            } else if (resp.data == act_error.OTA) {
                client.suspect();
                client.suspect();
            }
            aws(resp.state, resp.data);
            return;
        }
        let query = await select([rooms.roomID], rooms)
            .where(eq(rooms.name, data.room))
            .query(db);
        let roomID = (query[0] ?? {})[rooms.roomID];
        if (typeof roomID != "number" || roomID < 0) {
            client.suspect();
            return void aws("error", act_error.ROOM_NOT_EXISTS);
        }
        if (await isRoomFull(roomID)) return void aws("error", act_error.ROOM_USER_LIMIT);
        // TODO: Make Transaktion when possible
        await remove(roomOTAs)
            .where(or(
                eq(roomOTAs.usesLeft, 0),
                and(
                    le(roomOTAs.expires, uts()),
                    geq(roomOTAs.expires, 0)
                )
            ))
            .query(db);
        let req = await update(roomOTAs)
            .set(roomOTAs.usesLeft, minus(roomOTAs.usesLeft, 1))
            .where(eq(roomOTAs.token, data.token))
            .query(db);
        await remove(roomOTAs)
            .where(eq(roomOTAs.usesLeft, 0))
            .query(db);
        if (req.affectedRows == 0) {
            client.suspect();
            return void aws("error", act_error.OTA);
        }
        try {
            let queryx = await insert(
                roomMembers.roomID,
                roomMembers.name,
                roomMembers.server,
                roomMembers.admin,
                roomMembers.confirmed,
            ).add(
                roomID,
                client.name,
                client.state == STATE.remote ? client.server.tag : "local",
                0,
                true,
            ).query(db);
            aws("ok", "");
            /*if (queryx.affectedRows > 0) {
                aws("ok", "");
            } else {
                aws("error", act_error.DUPLICATE);
            }*/
        } catch (error) {
            aws("ok", "");
            //aws("error", act_error.DUPLICATE);
        }

    }
};

export const joinPublicRoom: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "joinPublicRoom", data);
            if (resp.state == "ok") {
                try {
                    await insert(remoteRooms.accID, remoteRooms.server, remoteRooms.room, remoteRooms.confirmed)
                        .add(client.accID, data.server, data.room, true)
                        .query(db);
                } catch (error) { }
            }
            aws(resp.state, resp.data);
            return;
        }
        let query = await select([rooms.roomID, rooms.visibility], rooms)
            .where(eq(rooms.name, data.room))
            .query(db);
        let roomID = (query[0] ?? {})[rooms.roomID];
        let visibility = (query[0] ?? {})[rooms.visibility];
        if (typeof roomID != "number" || roomID < 0 || typeof visibility != "number") {
            return void aws("error", act_error.ROOM_NOT_EXISTS);
        }
        if (((client.state == STATE.client) && (visibility < 1)) || ((client.state == STATE.remote) && (visibility < 2))) {
            return void aws("error", act_error.ROOM_NOT_EXISTS);
        }
        if (await isRoomFull(roomID)) return void aws("error", act_error.ROOM_USER_LIMIT);
        try {
            let queryx = await insert(
                roomMembers.roomID,
                roomMembers.name,
                roomMembers.server,
                roomMembers.admin,
                roomMembers.confirmed,
            ).add(
                roomID,
                client.name,
                client.state == STATE.remote ? client.server.tag : "local",
                0,
                true,
            ).query(db);
            aws("ok", "");
            /*if (queryx.affectedRows > 0) {
                aws("ok", "");
            } else {
                aws("error", act_error.DUPLICATE);
            }*/
        } catch (error) {
            aws("ok", "");
            //aws("error", act_error.DUPLICATE);
        }
    }
};

export const getRoomOTAs: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "getRoomOTAs", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.OTA);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        await remove(roomOTAs)
            .where(or(
                eq(roomOTAs.usesLeft, 0),
                and(
                    le(roomOTAs.expires, uts()),
                    geq(roomOTAs.expires, 0)
                )
            ))
            .query(db);
        let req = await select([roomOTAs.token, roomOTAs.name, roomOTAs.expires, roomOTAs.usesLeft], roomOTAs)
            .where(eq(roomOTAs.roomID, roomID))
            .query(db);
        aws("ok", req.map(d => ({
            token: d[roomOTAs.token],
            name: d[roomOTAs.name],
            expires: Number(d[roomOTAs.expires]),
            usesLeft: d[roomOTAs.usesLeft],
        })));
    }
};

export const addRoomOTA: Act = { // or change it, primary key is room and token
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        token: "string-256",
        name: "string-256",
        expires: "number",
        usesLeft: "number",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "addRoomOTA", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.OTA);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        try {
            let resp = await insert(roomOTAs.roomID, roomOTAs.token, roomOTAs.name, roomOTAs.expires, roomOTAs.usesLeft)
                .add(roomID, data.token, data.name, data.expires, data.usesLeft)
                .query(db);
            if (resp.affectedRows == 0) throw new Error("insertion fail");
        } catch (error) {
            await update(roomOTAs)
                .set(roomOTAs.expires, data.expires)
                .set(roomOTAs.usesLeft, data.usesLeft)
                .set(roomOTAs.name, data.name)
                .where(and(
                    eq(roomOTAs.token, data.token),
                    eq(roomOTAs.roomID, roomID)
                )).query(db);
        }
        aws("ok", "");
    }
};

export const deleteRoomOTA: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        token: "string"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "deleteRoomOTA", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.OTA);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        await remove(roomOTAs)
            .where(and(
                eq(roomOTAs.roomID, roomID),
                eq(roomOTAs.token, data.token)
            )).query(db);
        aws("ok", "");
    }
};

export const inviteUser: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "name-100",
        roomServer: "string",
        name: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.roomServer)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.roomServer, "inviteUser", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.OTA);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);

        let userServer = data.server as string;
        if (!checkSelfTag(userServer)) {
            let userOutbagServer: outbagServer;
            try {
                userOutbagServer = await outbagURLfromTag(userServer);
            } catch (error) {
                return void aws("error", act_error.ACCOUNT_NOT_EXISTS);
            }

            let resp = await fetchRemoteAsServer(userOutbagServer, "invite", data);
            if (resp.state == "error") {
                client.suspect();
                client.suspect();
                return void aws("error", act_error.ACCOUNT_NOT_EXISTS);
            }
        } else {
            userServer = "local";
            let req = await select([accounts.accID], accounts)
                .where(eq(accounts.name, data.name))
                .query(db);
            if (req.length == 0) {
                client.suspect();
                return void aws("error", act_error.ACCOUNT_NOT_EXISTS);
            }
        }
        // on roomServer
        let req = await insert(roomMembers.roomID, roomMembers.server, roomMembers.name, roomMembers.admin, roomMembers.confirmed)
            .add(roomID, userServer, data.name, false, false)
            .query(db);
        if (req.affectedRows > 0) aws("ok", "");
        else {
            client.suspect();
            aws("error", act_error.MEMBER_EXISTS);
        }
    }
};

export const confirmRoom: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "name-100",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "confirmRoom", data);
            if (resp.state == "ok") {
                try {
                    let query = await insert(remoteRooms.accID, remoteRooms.server, remoteRooms.room, remoteRooms.confirmed)
                        .add(client.accID, resp.server, data.room, true)
                        .query(db);
                    if (query.affectedRows == 0) throw new Error("insertion error");
                } catch (error) {
                    await update(remoteRooms)
                        .set(remoteRooms.confirmed, true)
                        .where(and(
                            eq(remoteRooms.accID, client.accID),
                            eq(remoteRooms.server, resp.server),
                            eq(remoteRooms.room, data.room)
                        )).query(db);
                }
            }
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) return void aws("error", act_error.NOT_IN_ROOM);
        let req = await update(roomMembers)
            .set(roomMembers.confirmed, true)
            .where(and(
                eq(roomMembers.roomID, roomID),
                eq(roomMembers.name, client.name),
                eq(roomMembers.server, client.state == STATE.client ? "local" : client.server.tag)
            )).query(db);
        if (req.affectedRows > 0) aws("ok", "");
        else aws("error", act_error.MEMBER_NOT_EXISTS);
    }
}

export const kickMember: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        roomServer: "string",
        name: "string",
        server: "string",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.roomServer)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "kickMember", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.MANAGE_MEMBERS);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        let req = await remove(roomMembers)
            .where(and(
                eq(roomMembers.roomID, roomID),
                eq(roomMembers.name, data.name),
                eq(roomMembers.server, checkSelfTag(data.server) ? "local" : data.server),
                or(
                    not(eq(roomMembers.server, "local")),
                    not(exists(
                        select([accounts.accID], innerJoinOn(accounts, rooms, eq(accounts.accID, rooms.owner)))
                            .where(and(
                                eq(rooms.roomID, roomMembers.roomID),
                                eq(accounts.name, roomMembers.name)
                            ))
                    ))
                )
            )).query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            aws("error", act_error.MEMBER_NOT_EXISTS)
        }
    }
};

export const setAdminStatus: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        roomServer: "string",
        name: "string",
        server: "string",
        admin: "boolean",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.roomServer)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.roomServer, "setAdminStatus", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.CHANGE_ADMIN);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        let req = await update(roomMembers)
            .set(roomMembers.admin, data.admin)
            .where(and(
                eq(roomMembers.roomID, roomID),
                eq(roomMembers.name, data.name),
                eq(roomMembers.server, checkSelfTag(data.server) ? "local" : data.server)
            )).query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            aws("error", act_error.MEMBER_NOT_EXISTS);
        }
        sendRoomListeners(roomID, LISTENER_TYPE.ROOMINFO, LISTENER_ACTION.CHANGE, []);
    }
};

export const leaveRoom: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        force: "boolean",
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "leaveRoom", data);
            if (resp.state == "ok" || resp.data == act_error.NOT_IN_ROOM || data.force) {
                await remove(remoteRooms)
                    .where(and(
                        eq(client.accID, remoteRooms.accID),
                        eq(data.server, remoteRooms.server),
                        eq(data.room, remoteRooms.room),
                    )).query(db);
            }
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isInRoom(data.room);
        if (roomID == -1) return void aws("error", act_error.NOT_IN_ROOM);
        let req = await remove(roomMembers)
            .where(and(
                eq(roomMembers.roomID, roomID),
                eq(roomMembers.name, client.name),
                eq(roomMembers.server, client.state == STATE.client ? "local" : client.server.tag),
                or(
                    not(eq(roomMembers.server, "local")),
                    not(exists(
                        select([accounts.accID], innerJoinOn(accounts, rooms, eq(accounts.accID, rooms.owner)))
                            .where(and(
                                eq(rooms.roomID, roomMembers.roomID),
                                eq(accounts.name, roomMembers.name)
                            ))
                    ))
                )
            )).query(db);
        if (req.affectedRows > 0) {
            aws("ok", "");
        } else {
            aws("error", act_error.OWNER);
        }
    }
};

export const setVisibility: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        visibility: "number"  //0 is not, 1 only to clients, 2 or bigger everywhere
    },
    func: async (client, data, aws) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "setVisibility", data);
            aws(resp.state, resp.data);
            return;
        }
        if (!([0, 1, 2]).includes(data.visibility)) return void aws("error", act_error.DATA);
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.OTA);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        let req = await update(rooms)
            .set(rooms.visibility, data.visibility)
            .where(eq(rooms.roomID, roomID))
            .query(db);
        aws("ok", "");
    }
};
export const setRoomRight: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        rights: "number", //see permissions.ts
    },
    func: async (client, data, aws) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "setRoomRight", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.CHANGE_ADMIN);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        let req = await update(rooms)
            .set(rooms.rights, data.rights)
            .where(eq(rooms.roomID, roomID))
            .query(db);
        aws("ok", "");
        sendRoomListeners(roomID, LISTENER_TYPE.ROOMINFO, LISTENER_ACTION.CHANGE, []);
    }
};

export const changeRoomMeta: Act = {
    state: STATE.client | STATE.remote,
    right: 0,
    data: {
        room: "string",
        server: "string",
        title: "string-255",
        description: "string-255",
        icon: "string-255"
    },
    func: async (client: Client, data: any, aws: (code: string, data: any) => void) => {
        if (!checkSelfTag(data.server)) {
            if (client.state != STATE.client) return void aws("error", act_error.RECURSION);
            let resp = await client.pass(data.server, "changeRoomMeta", data);
            aws(resp.state, resp.data);
            return;
        }
        let roomID = await client.isRoomAdmin(data.room, ROOM_RIGHTS.CHANGE_META);
        if (roomID == -1) return void aws("error", act_error.NOT_ROOM_ADMIN);
        let req = await update(rooms)
            .set(rooms.title, data.title)
            .set(rooms.description, data.description)
            .set(rooms.icon, data.icon)
            .where(eq(rooms.roomID, roomID))
            .query(db);
        aws("ok", "");
        sendRoomListeners(roomID, LISTENER_TYPE.ROOMINFO, LISTENER_ACTION.CHANGE, []);
    }
};