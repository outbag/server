import { alias, eq, count, select, innerJoinOn } from "dblang";
import { accounts, db, listCategories, listItems, listProducts, roomMembers, rooms } from "../sys/db.js";

export const isRoomFull = async (roomID: number) => {
    let currentCount = alias(
        select([count(roomMembers.roomMemberID)], roomMembers)
            .where(eq(roomMembers.roomID, roomID)),
        "currentCount"
    ) as any;
    let maxCount = alias(
        select([accounts.maxUsersPerRoom],
            innerJoinOn(accounts, rooms, eq(accounts.accID, rooms.owner)))
            .where(eq(rooms.roomID, roomID)),
        "maxCount"
    ) as any;
    let req = await select([currentCount, maxCount], null)
        .query(db);
    if (req[0][maxCount] == -1) return false;
    return req[0][currentCount] >= req[0][maxCount];
};

export const isRoomDataFull = async (roomID: number) => {
    let currentCatCount = alias(
        select([count(listCategories.listCatID)], listCategories)
            .where(eq(listCategories.roomID, roomID)),
        "currentCatCount"
    ) as any;
    let currentProdCount = alias(
        select([count(listProducts.listProdID)], listProducts)
            .where(eq(listProducts.roomID, roomID)),
        "currentProdCount"
    ) as any;
    let currentItemCount = alias(
        select([count(listItems.listItemID)], listItems)
            .where(eq(listItems.roomID, roomID)),
        "currentItemCount"
    ) as any;
    let maxCount = alias(
        select([accounts.maxRoomSize],
            innerJoinOn(accounts, rooms, eq(accounts.accID, rooms.owner)))
            .where(eq(rooms.roomID, roomID)),
        "maxCount"
    ) as any;
    let req = await select([currentCatCount, currentProdCount, currentItemCount, maxCount], null)
        .query(db);
    if (req[0][maxCount] == -1) return false;
    return req[0][currentCatCount] + req[0][currentProdCount] + req[0][currentItemCount] >= req[0][maxCount];
};

export const canCreateRoom = async (accID: number) => {
    let currentCount = alias(
        select([count(rooms.roomID)], rooms)
            .where(eq(rooms.owner, accID)),
        "currentCount"
    ) as any;
    let maxCount = alias(
        select([accounts.maxRooms], accounts)
            .where(eq(accounts.accID, accID)),
        "maxCount"
    ) as any;
    let req = await select([currentCount, maxCount], null)
        .query(db);
    if (req[0][maxCount] == -1) return true;
    return req[0][currentCount] < req[0][maxCount];
};

export const isCategoryInRoom = async (roomID: number, catID: number) => {
    let res = await select([listCategories.roomID], listCategories)
        .where(eq(listCategories.listCatID, catID))
        .query(db);
    return res.length > 0 && res[0][listCategories.roomID] == roomID;
};
export const isProductInRoom = async (roomID: number, prodID: number) => {
    let res = await select([listProducts.roomID], listProducts)
        .where(eq(listProducts.listProdID, prodID))
        .query(db);
    return res.length > 0 && res[0][listProducts.roomID] == roomID;
};
export const isItemInRoom = async (roomID: number, itemID: number) => {
    let res = await select([listItems.roomID], listItems)
        .where(eq(listItems.listItemID, itemID))
        .query(db);
    return res.length > 0 && res[0][listItems.roomID] == roomID;
};