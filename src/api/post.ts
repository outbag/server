import express from "express";
import { Act, checktype, Client, STATE } from "./user.js";
import { debug, error } from "../sys/log.js";
import * as authorization from 'auth-header';
import * as importActs from "./acts.js"
import { and, eq, select } from "dblang";
import { accounts } from "../sys/db.js";
import { db } from "../sys/db.js"
import { sha256 } from "../sys/crypto.js";
import { get64, uts } from "../sys/tools.js";
import { addShutdownTask } from "nman";
import { suspectRequest } from "../sys/bruteforce.js";
import { checkSelfTag, outbagServer } from "../server/outbagURL.js";
import { selfTag } from "../sys/selfTag.js";
import { act_error } from "../server/errors.js"

let acts = importActs as { [key: string]: Act };

let tempTokens: { [key: string]: postClient } = {};

let activePost = false;
export const activatePost = () => { activePost = true; };

export const addPostMethods = (server: express.Express) => {
    for (const act in acts) {
        let methode = acts[act];
        server.post("/api/" + act, async (req: suspectRequest, res) => {
            if (!activePost) {
                res.status(500);
                res.send("not active");
                return;
            }
            debug("POST", "act:", act, "received:", req.body);
            const aws = (state: string, data: any) => {
                debug("POST", "send:", state, data);
                res.status(state == "error" ? 400 : 200);
                res.json({ data });
            };
            let client: postClient | null = null;
            try {
                let auth = authorization.parse(req.headers["authorization"] ?? "");
                if (auth.scheme == "outbagServer") {
                    debug("POST", "auth: outbag Server");
                    if (
                        typeof auth.token == "string"
                        && tempTokens[auth.token] != null
                        && typeof auth?.params?.as == "string"
                        && tempTokens[auth.token].client.state == STATE.server
                    ) {
                        let serverClient = tempTokens[auth.token];
                        client = new postClient(req.socket.remoteAddress ?? "");
                        client.client.name = auth.params.as;
                        client.client.server = serverClient.client.server;
                        client.client.state = STATE.remote;
                    } else {
                        if (req.suspect) req.suspect();
                        aws("error", act_error.SERVER_TOKEN);
                        return;
                    }
                } else if (auth.token != null && typeof auth.token == "string") {
                    debug("POST", "auth: temp Token");
                    if (tempTokens[auth.token] != null) {
                        client = tempTokens[auth.token];
                    } else {
                        if (req.suspect) req.suspect();
                        aws("error", act_error.TOKEN);
                        return;
                    }
                } else if (
                    typeof auth?.params?.name == "string"
                    && typeof auth?.params?.server == "string"
                    && typeof auth?.params?.accountKey == "string"
                ) {
                    debug("POST", "auth: credentials");
                    client = new postClient(req.ip);
                    client.client.name = auth?.params?.name;
                    let serverTag = auth?.params?.server;
                    if (!checkSelfTag(serverTag)) {
                        debug("POST", "auth: bad server tag:", serverTag);
                        aws("error", act_error.DATA);
                        return;
                    }
                    client.client.server = new outbagServer(serverTag, selfTag.host, selfTag.path, selfTag.port);
                    let accountKey = auth?.params?.accountKey;

                    let query = await select([accounts.accID, accounts.accountKey, accounts.accountKeySalt], accounts)
                        .where(and(
                            eq(accounts.name, client.client.name),
                            eq(accounts.deleted, 0)
                        ))
                        .query(db);

                    if (query.length == 0 || query[0][accounts.accountKey] != sha256((query[0][accounts.accountKeySalt] ?? '') + accountKey)) {
                        if (req.suspect) req.suspect();
                        aws("error", act_error.AUTH);
                        return;
                    }
                    client.client.accID = query[0][accounts.accID];
                    client.client.state = STATE.client;
                }
            } catch (error) {

            }

            if (client == null) client = new postClient(req.socket.remoteAddress ?? "");
            let send = false;
            await client.runAct(methode, req.body, (state: string, data: any) => {
                aws(state, data);
                send = true;
            });
            if (!send) aws("error", act_error.SERVER);
        });
    }
}

export class postClient {
    lastReq = uts();
    client: Client;
    constructor(ip: string, client: Client | null = null) {
        if (client === null) client = new Client(ip, this);
        this.client = client;
    }
    async runAct(act: Act, json: any, aws: (state: string, data: any) => void) {
        this.lastReq = uts();
        try {
            let { state, data, right, func } = act;
            if (!(state & this.client.state)) {
                aws("error", act_error.WRONG_STATE);
                this.client.suspect();
                return;
            }
            if (typeof json.data == "undefined") {
                aws("error", act_error.DATA);
                return;
            }
            if (data) {
                for (let d in data) {
                    if (!checktype(json.data[d], data[d])) {
                        debug("POST", "Data check error. Key: ", d, "; Type:", data[d], "; Value:", json.data[d]);
                        aws("error", act_error.DATA);
                        return;
                    }
                }
            }
            if (right && !(await this.client.checkRight(right))) {
                aws("error", act_error.RIGHT);
                this.client.suspect();
                return;
            }
            var send = false;
            try {
                await func(this.client, json.data, (state, data = "") => {
                    aws(state, data);
                    send = true;
                });
            } catch (e) {
                error("POST", "act error:", e);
            }

            if (!send) {
                aws("error", act_error.SERVER);
            }
        } catch (error) {
            aws("error", act_error.SERVER);
        }
    }
}

export const addTempToken = (client: postClient) => {
    let token = get64(128);
    if (tempTokens[token] != null) token = get64(128);
    if (tempTokens[token] != null) token = get64(128);
    if (tempTokens[token] != null) return false;
    tempTokens[token] = client;
    return token;
};

let cancleClear = setInterval(() => {
    let keys = Object.keys(tempTokens);
    for (let i = 0; i < keys.length; i++) {
        const c = tempTokens[keys[i]];
        if (c.lastReq + 60 * 60 * 1 < uts()) {
            delete tempTokens[keys[i]];
        }
    }
}, 1000);

addShutdownTask(() => void clearInterval(cancleClear));