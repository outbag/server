import ws from "ws";
import http from "http";
import { bruteforcecheck } from "../sys/bruteforce.js";
import { Act, checktype, Client } from "./user.js";
import { debug, error } from "../sys/log.js";
import * as importActs from "./acts.js"
import { act_error } from "../server/errors.js";
import { addRoomListener, removeRoomListener } from "./listener.js";

let acts = importActs as { [key: string]: Act };

let activeWS = false;
export const activateWS = () => { activeWS = true; };

export const wsOnConnection = (socket: ws.WebSocket, req: http.IncomingMessage) => {
    let ip = req.socket.remoteAddress;
    if (!activeWS || !bruteforcecheck(ip ?? "")) {
        debug("WebSocket", "Rejected ip for bruteforce suspicion:", ip);
        socket.close();
        return;
    }
    debug("WebSocket", "Opend new WebSocket from ip:", ip);
    new wsClient(socket, req);
}

let clients: wsClient[] = [];

export class wsClient {
    socket: ws.WebSocket;
    open = true;
    activeRequests = 0;
    client: Client;
    constructor(socket: ws.WebSocket, req: http.IncomingMessage) {
        this.client = new Client(req.socket.remoteAddress ?? "", this);
        this.socket = socket;

        clients.push(this);

        socket.on("message", async (msg: any) => {
            try {
                this.activeRequests++;
                let msgStr = msg.toString();
                let json = JSON.parse(msgStr) as { act: string, id: number, data: any };
                debug("WebSocket", "reveived:", json);
                if (!this.open) {
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.CLOSED
                    }));
                    debug("WebSocket", "send:", "error", "closed");
                    return;
                }
                if (!bruteforcecheck(this.client.ip)) {
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.BRUTEFORCE
                    }));
                    debug("WebSocket", "send:", "error", "bruteforce");
                    return;
                }
                if (typeof json.act != "string") {
                    return;
                }
                if (acts[json.act] == null) {
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.NOT_FOUND
                    }));
                    debug("WebSocket", "send:", "error", "notfound");
                    return;
                }
                let { state, data, right, func } = acts[json.act];
                if (!(state & this.client.state)) {
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.WRONG_STATE
                    }));
                    debug("WebSocket", "send:", "error", "wrongstate");
                    this.client.suspect();
                    return;
                }
                if (json.data === null) {
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.DATA
                    }));
                    debug("POST", "send:", "error", "data");
                    return;
                }
                if (data) {
                    for (let d in data) {
                        if (!checktype(json.data[d], data[d])) {
                            socket.send(JSON.stringify({
                                id: json.id,
                                state: "error",
                                data: act_error.DATA
                            }));
                            debug("WebSocket", "Data check error. Key: ", d, "; Type:", data[d], "; Value:", json.data[d]);
                            debug("WebSocket", "send:", "error", "data");
                            return;
                        }
                    }
                }
                if (right && !(await this.client.checkRight(right))) {
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.RIGHT
                    }));
                    debug("WebSocket", "send:", "error", "right");
                    this.client.suspect();
                    return;
                }
                var send = false;
                try {
                    await func(this.client, json.data, (state, data = "") => {
                        debug("WebSocket", "send:", state, data);
                        socket.send(JSON.stringify({ id: json.id, state, data }, (k, v) => typeof v == "bigint" ? Number(v) : v));
                        send = true;
                    });
                } catch (e) {
                    error("WebSocket", "act error:", e);
                }

                if (!send) {
                    debug("WebSocket", "send:", "error", "server");
                    socket.send(JSON.stringify({
                        id: json.id,
                        state: "error",
                        data: act_error.SERVER
                    }));
                }
            } catch (error) {
                debug("WebSocket", "error:", error);
                socket.send("error");
            } finally {
                this.activeRequests--;
            }
        });

        socket.on('close', () => {
            this.unlistenAllRooms();
            this.open = false;
            var i = clients.indexOf(this);
            delete clients[i];
            if (i >= 0) {
                clients.splice(i, 1);
            }
        });
    }

    close(ms: number) {
        return new Promise<void>(res => {
            this.unlistenAllRooms();
            if (this.activeRequests == 0) {
                this.socket.close();
                return void res();
            }
            setTimeout(() => {
                this.socket.close();
                res();
            }, Math.max(100, ms));
        });
    }

    sendFromServer(id: any, data: any): Promise<boolean> {
        if (this.socket.readyState != ws.OPEN) {
            return Promise.resolve(false);
        }
        return new Promise((res, rej) => {
            this.socket.send(JSON.stringify({
                id,
                state: "server",
                data: data
            }), (err) => {
                if (err) return void rej(false);
                return void res(true);
            });
        });


    }

    listenRoom(roomID: number, id: any, room: string, server: string) {
        addRoomListener(this, id, roomID, room, server);
    }
    unlistenRoom(roomID: number) {
        removeRoomListener(this, roomID);
    }
    unlistenAllRooms() {
        removeRoomListener(this);
    }
}

export const closeWebSocket = async () => {
    for (let i = 0; i < clients.length; i++) {
        clients[i].open = false;
    }
    var now = performance.now();
    for (let i = 0; i < clients.length; i++) {
        await clients[i].close(now - performance.now() + 25000);
    }
};