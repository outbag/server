import { eq, ge, select } from "dblang";
import express, { Request, Response } from "express";
import { suspectRequest } from "../sys/bruteforce.js";
import { oConf } from "../sys/config.js";
import { accounts, db, rooms } from "../sys/db.js";
import { error, logList } from "../sys/log.js";
import { getSettings, SETTINGS } from "../sys/settings.js";
import logHTML from "../html/log.js";
import { selfTag } from "../sys/selfTag.js";

var Methods: ([string, (req: Request, res: Response) => Promise<void>])[] = [
    ["users", async (req: Request, res: Response) => {
        let resp = await select([accounts.name], accounts)
            .where(eq(accounts.viewable, true))
            .query(db);
        res.json(resp.map(d => d[accounts.name]));
    }],
    ["server/publicKey", async (req: Request, res: Response) => {
        let publicKey = await getSettings(SETTINGS.publicKey);
        let expires = await getSettings(SETTINGS.certExpires);
        if (publicKey && expires) {
            res.json({
                publicKey,
                expires
            });
        }
    }],
    ["server/state", async (req: Request, res: Response) => {
        res.json({
            debug: global.debug
        });
    }],
    ["isUser/:user", async (req: suspectRequest, res: Response) => {
        var user = req.params.user;
        let data = await select([accounts.accID], accounts)
            .where(eq(accounts.name, user))
            .query(db);
        if (data.length == 0) {
            if (req.suspect) req.suspect();
            res.send("error");
        } else {
            res.send("ok");
        }
    }],
    ["publicRooms", async (req: Request, res: Response) => {
        let data = await select([rooms.name, rooms.title, rooms.description, rooms.icon], rooms)
            .where(ge(rooms.visibility, 1))
            .query(db);
        var out = data.map(d => {
            let name = d[rooms.name];
            let title = d[rooms.title];
            let description = d[rooms.description];
            let icon = d[rooms.icon];
            if (name != null && title != null && description != null && icon != null) {
                return { name, server: selfTag.tag, title, description, icon, debug: global.debug };
            }
            return null;
        });
        res.json(out.filter(d => d != null));
    }],
    ["log/json", async (req: Request, res: Response) => {
        if (global.provideLog == 0) return void res.status(500).send("disabled");
        res.json(logList);
    }],
    ["log/ansi", async (req: Request, res: Response) => {
        if (global.provideLog == 0) return void res.status(500).send("disabled");
        res.send(logList.map(([id, type, date, name, args]) => {
            let out = `\x1b[33m${date}\x1b[0m\x1b[1m`;
            if (type == "debug") {
                out += `\x1b[1m\x1b[32m [${name}]: \x1b[0m`;
            } else if (type == "log") {
                out += `\x1b[1m\x1b[36m [${name}]: \x1b[0m`;
            } else if (type == "warn") {
                out += `\x1b[1m\x1b[36m [${name}]: \x1b[0m`;
            } else {
                out += ` \x1b[1m\x1b[41m[${name}]: \x1b[0m\x1b[41m`;
            }
            out += args.map(a => `${typeof a == 'object' ? JSON.stringify(a, (k, v) => typeof v == "bigint" ? Number(v) : v) : a}`).join(" ");
            out += "\x1b[0m";
            return out;
        }).join("\x1b[0m\n"));
    }],
    ["log/html", async (req: Request, res: Response) => {
        res.send(logHTML);
    }],
];

export const addGetMethods = (server: express.Express) => {
    server.get("/.well-known/outbag/server", (req, res) => {
        res.json({
            port: oConf.get("System", "PORTexposed"),
            path: oConf.get("System", "PATHexposed"),
        })
    });
    Methods.forEach((d) => void server.get("/api/" + d[0], async (req, res) => {
        try {
            await d[1](req, res);
            if (!res.headersSent) res.status(500).send();
        } catch (e) {
            error("API", "Get Request Error:", e);
            res.status(500).send();
        }
    }));
}