import { and, eq, innerJoinUsing, naturalJoin, select, update } from "dblang";
import { accounts, db, roomMembers, rooms } from "../sys/db.js";
import { addBruteforcePotantial } from "../sys/bruteforce.js";
import { outbagServer, outbagURLfromTag } from "../server/outbagURL.js";
import { fetchRemoteAs } from "./server.js";
import { debug } from "../sys/log.js";
import { act_error } from "../server/errors.js";
import { wsClient } from "./ws.js";
import { postClient } from "./post.js";

export const STATE = {
    no: 0b00001,
    remoteP: 0b000010,
    remote: 0b000100,
    client: 0b001000,
    serverP: 0b010000,
    server: 0b100000,
};

export const MODE = {
    ws: 0b01,
    post: 0b10,
    both: 0b11,
};

export type Act = {
    state: number,
    right: number,
    data: {
        [key: string]: any
    },
    func: (client: Client, data: any, aws: (code: string, data: any) => void) => Promise<void>;
};

export class Client {
    name: string = "";
    server: outbagServer;
    ip: string = "";
    state: number = STATE.no;

    accID: number = -1;
    challenge: string = "";
    remoteKey: string = "";
    
    connectionClient: wsClient | postClient;

    constructor(ip: string, client: wsClient | postClient) {
        this.connectionClient = client;
        this.server = new outbagServer("", "", "", "");
        this.ip = ip;
    }

    suspect() {
        addBruteforcePotantial(this.ip);
    }

    async pass(server: string, act: string, data: any) {
        debug("Client", "passing act to remote:", server);
        try {
            let s = await outbagURLfromTag(server);
            let resp = await fetchRemoteAs(s, this.name, act, data) as {
                state: string,
                data: any,
                server: string | null
            };
            resp.server = s.tag;
            return resp;
        } catch (error) {
            return {
                state: "error",
                data: act_error.REMOTE,
                server: null
            }
        }

    }

    async isInRoom(name: string): Promise<number> {
        if (this.state != STATE.client && this.state != STATE.remote) return -1;
        let query = await select([rooms.roomID], innerJoinUsing(rooms, roomMembers, rooms.roomID, roomMembers.roomID))
            .where(and(
                eq(rooms.name, name),
                eq(roomMembers.name, this.name),
                eq(roomMembers.server, this.state == STATE.client ? "local" : this.server.tag)
            ))
            .query(db);
        if (query.length == 0) return -1;
        return query[0][rooms.roomID];
    }
    async isRoomAdmin(name: string, roomRightRequires: number): Promise<number> {
        if (this.state != STATE.client && this.state != STATE.remote) return -1;
        let query = await select([rooms.roomID, rooms.owner, rooms.rights, roomMembers.admin], innerJoinUsing(rooms, roomMembers, rooms.roomID, roomMembers.roomID))
            .where(and(
                eq(rooms.name, name),
                eq(roomMembers.name, this.name),
                eq(roomMembers.server, this.state == STATE.client ? "local" : this.server.tag)
            ))
            .query(db);
        if (query.length == 0) return -1;
        if (
            !query[0][roomMembers.admin]
            && query[0][rooms.owner] != this.accID
            && !(query[0][rooms.rights] & roomRightRequires)
        ) return -1;
        return query[0][rooms.roomID];
    }

    async checkRight(right: number) {
        try {
            if (right == 0) return true;
            let data = await select([accounts.rights], accounts)
                .where(eq(accounts.accID, this.accID))
                .query(db);
            return ((data[0][accounts.rights]) & right) == right;
        } catch (e) {

        }
        return false;
    };

    async checkAnyRight(right: number){
        try {
            if (right == 0) return true;
            let data = await select([accounts.rights], accounts)
                .where(eq(accounts.accID, this.accID))
                .query(db);
            return ((data[0][accounts.rights]) & right) > 0;
        } catch (e) {

        }
        return false;
    }

    async setRights(rights: number) {
        await update(accounts)
            .set(accounts.rights, rights)
            .where(eq(accounts.accID, this.accID))
            .query(db);
    };
}


export function checktype(data: any, type: string) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (typeof data == type) {
        return true;
    } else if (type == "nullpointer") {
        return data == null || (typeof data == "number" && Number.isInteger(data) && data >= 0);
    } else if (type == "int" && typeof data == "number" && Number.isInteger(data)) {
        return true;
    } else if (type == "email" && typeof data == "string" && re.test(data)) {
        return true;
    } else if (type == "name" && typeof data == "string" && /^[a-zA-Z0-9\-_.]+$/.test(data)) {
        return true;
    } else if (typeof data == "string" && type.startsWith("name-") && /^[a-zA-Z0-9\-_.]+$/.test(data)) {
        return parseInt(type.split("-")[1]) >= data.length;
    } else if (typeof data == "string" && type.startsWith("string-")) {
        return parseInt(type.split("-")[1]) >= data.length;
    } else if (type.startsWith("array-") && Array.isArray(data)) {
        let ttype = type.substring(6);
        for (let i = 0; i < data.length; i++) {
            if (!checktype(data[i], ttype)) return false;
        }
        return true;
    } else if (type == "any") {
        return true;
    } else {
        return false;
    }
}