export default /*html*/ `
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Outbag log</title>
    <style>
        body {
            color: white;
            background-color: black;
        }

        pre {
            white-space: pre-wrap;
        }

        .date {
            color: rgb(187, 187, 0);
        }

        .log {
            font-weight: bold;
            color: rgb(0, 187, 187);
        }

        .warn {
            font-weight: bold;
            color: rgb(0, 187, 187);
        }

        .debug {
            font-weight: bold;
            color: rgb(0, 187, 0);
        }

        .error {
            font-weight: bold;
            background-color: rgb(187, 0, 0);
        }

        .errorText {
            background-color: rgb(187, 0, 0);
        }

        .string {
            color: rgb(0, 187, 0);
        }

        .number {
            color: rgb(187, 187, 0);
        }

        .boolean {
            /*font-weight: bold;*/
            color: rgb(187, 0, 187);
        }

        .object {
            color: rgb(0, 0, 187);
            font-weight: bold;
        }
    </style>
</head>

<body>
    <pre></pre>
    <script>
        let pre = document.querySelector("pre");

        let loaded = [];

        function parseJSON(json, depth = 0){
            let elems = [];
            function addSpaces(num, text = "") {
                let space = document.createElement("span");
                space.innerText = new Array(num).fill(" ").join("")+text;
                elems.push(space);
            }

            function addBreak() {
                elems.push(document.createElement("br"));
            }

            function addData(data){
                if(typeof data == "object" && data != null){
                    elems.push(...parseJSON(data, depth+2));
                }else{
                    let line = document.createElement("span");
                    if(typeof data == "string") line.innerText = "'"+data+"'";
                    else line.innerText = data == null ? "null" : data;
                    line.className = typeof data;
                    elems.push(line);
                }                
            }

            if(Array.isArray(json)){
                addSpaces(0, "[");
                let doBreak = JSON.stringify(json).length > 64;                
                json.forEach((e,i) => {
                    if(i == 0 && doBreak) addBreak();
                    if(doBreak) addSpaces(depth+2);

                    addData(e);
                    
                    if(i+1 < json.length){
                        let k = document.createElement("span");
                        k.innerText = ", ";
                        elems.push(k);
                    }

                    if(doBreak) addBreak();
                });
                addSpaces(doBreak ? depth : 0, "]");
            }else{
                addSpaces(0, "{");
                let keys = Object.keys(json);
                let doBreak = JSON.stringify(json).length > 64;
                for(let i = 0; i < keys.length; i++){
                    if(i == 0 && doBreak) addBreak();
                    let key = keys[i];

                    if(doBreak) addSpaces(depth+2);
                    let keyElem = document.createElement("span");
                    keyElem.innerText = key + ": ";
                    elems.push(keyElem);

                    addData(json[key]);

                    if(i+1 < keys.length){
                        let k = document.createElement("span");
                        k.innerText = ", ";
                        elems.push(k);
                    }

                    if(doBreak) addBreak();
                }
                addSpaces(doBreak ? depth : 0, "}");
            }
            return elems;
        }

        function parse([id, type, date, name, args ]){
            let dateSpan = document.createElement("span");
            dateSpan.className = "date";
            dateSpan.innerText = date+" ";
            pre.appendChild(dateSpan);

            let nameSpan = document.createElement("span");
            nameSpan.className = type;
            nameSpan.innerText = "["+name+"]: ";
            pre.appendChild(nameSpan);

            let argsSpan = document.createElement("span");
            if(type=="error") argsSpan.className = "errorText";
            args.forEach(a => {
                if(typeof a != 'object' || a == null){
                    argsSpan.appendChild(document.createTextNode(a));
                }else{
                    argsSpan.append(...parseJSON(a));
                }
                argsSpan.appendChild(document.createTextNode(" "));
            });
            //argsSpan.innerText = args.map(a => typeof a == 'object' ? parseJSON(a) : a).join(" ");
            pre.appendChild(argsSpan);

            pre.appendChild(document.createElement("br"));
        }

        async function refresh(){
            try{
                let resp = await fetch("json");
                if(resp.status != 200){
                    throw new Error("can not get Data");
                }
                let json = await resp.json();

                let scrollPosition = document.documentElement.getBoundingClientRect().bottom;
                let heigth = window.innerHeight;

                let scroll = heigth + 50 >= scrollPosition;
                if(loaded.length == 0) pre.innerText = "";
                json.forEach(d=>{
                    if(loaded.includes(d[0])) return; 
                    loaded.push(d[0]);
                    parse(d);
                });
                if(scroll) document.documentElement.scrollIntoView(false);
            }catch(_){
                pre.innerText = "Outbag Log Api is disabled or Server is down.";
                loaded = [];
            }
            
        }

        setInterval(refresh, 1000);

    
    </script>
</body>

</html>
`;