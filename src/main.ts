declare global {
    var debug: boolean;
    var provideLog: number;
}
import express from "express";
import https from "https";
import http from "http";
import nman from "nman";
import cors from "cors";
import { WebSocketServer } from "ws";
import { Command } from "commander";
import { oConf } from "./sys/config.js";
import { generateTag } from "./sys/selfTag.js"
import { error, log, warn } from "./sys/log.js";
import { connectToDB } from "./sys/db.js";
import bruteforce from "./sys/bruteforce.js";
import { fullSetup, partiellSetup } from "./setup/config.js";
import { addGetMethods } from "./api/get.js";
import { activatePost, addPostMethods } from "./api/post.js";
import { activateWS, closeWebSocket, wsOnConnection } from "./api/ws.js";
import { startUpdateCert } from "./server/serverCerts.js";
import { wait } from "./sys/tools.js";
import { startChecks } from "./server/checks.js";

global.provideLog = 10;

const config = {
    version: "0.0.1"
};

const program = new Command();

program
    .name("Outbag Server")
    .description("The default way to host your own Outbag server.")
    .version(config.version);
program
    .option("-c, --config <path>", "Start the Outbag server with a config file.")
    .option("-d, --debug", "Start the Outbag server with more log output.")
    .option("-l, --log <number>", "Amount of log entires provided in get api! -1 = inifinity")
    .option("-s, --setup", "Start the Server Setup Tool")
    .action(({ config, debug, log: provideLog, setup }) => {
        let dofullSetup = false;
        global.debug = debug != null;
        global.provideLog = provideLog != null ? provideLog : 0;
        if (global.debug) {
            warn("System", "Starting in debug mode: Server should not be used in production!");
            process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";
        }
        if (config) {
            log("System", "Starting with config:", config);
            if (!oConf.connect(config)) dofullSetup = true;
        }

        if (setup || dofullSetup) {
            if (dofullSetup) fullSetup();
            else partiellSetup();
        } else {
            startServer();
        }
    });


program.parse();

const activeRequest = false;

var serverclose: (https.Server | http.Server | null) = null;
nman.addShutdownTask(() => new Promise(async (res, rej) => {
    await closeWebSocket();
    if (serverclose != null) {
        serverclose.keepAliveTimeout = 100;
        serverclose.close(() => res());
    } else {
        res();
    }
}), 10000);

async function startServer() {
    await connectToDB();

    const server = express();
    server.use(express.json({ limit: '1000mb' }));
    server.use(express.urlencoded({ limit: '1000mb', extended: false }));
    server.use(cors());
    server.use(bruteforce);
    addGetMethods(server);
    addPostMethods(server);
    if (oConf.get("ssl", "enable")) {
        log("Server", "SSL Enabled");
        oConf.readPathes(oConf.get("ssl", "privkey"), oConf.get("ssl", "cert"), oConf.get("ssl", "chain"))
            .then(([privkey, cert, chain]: any) => {
                const HTTPserver = chain != "" ?
                    https.createServer({ key: privkey, cert: cert, ca: chain }, server) :
                    https.createServer({ key: privkey, cert: cert }, server);
                const wssServer = new WebSocketServer({ server: HTTPserver });
                wssServer.on('connection', wsOnConnection);
                serverclose = HTTPserver.listen(oConf.get("System", "PORT"), () => {
                    complete_loaded();
                });
            }).catch(err => {
                error("Server", err);
                nman.shutdown();
            });
    } else {
        log("Server", "SSL Disabled");
        const HTTPserver = http.createServer({}, server);
        const wssServer = new WebSocketServer({ server: HTTPserver });
        wssServer.on('connection', wsOnConnection);
        serverclose = HTTPserver.listen(oConf.get("System", "PORT"), () => {
            complete_loaded();
        });
    }
}


async function complete_loaded() {
    await startUpdateCert();
    await wait(500);
    let succ = await generateTag();
    if (!succ) {
        error("Outbag", "Could not check own Server Tag. Remote-Auth may not work! Check if the Server is reachable and the config ist correct!");
        if (!global.debug) {
            await nman.shutdown();
            process.exit(1);
        }
        warn("Outbag", "Keep running due to debug Mode");
    }
    activatePost();
    activateWS();
    startChecks();
    log("Server", 'Listening...');
}