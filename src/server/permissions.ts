export const PERMISSIONS = {
    NONE:                        0b0000000000000000, // equal to no account or blocked account

    DEFAULT:                     0b0000000000000011, //default

    CAN_USE_API:                 0b0000000000000001,
    PROVIDE_CERT:                0b0000000000000010,

    VIEW_SERVER_STATS:           0b1111111110000000,
    MANAGE_OTA_TOKENS:           0b0000001000000000,
    MANAGE_SERVER_PRODUCT_LIST:  0b0000010000000000,
    SHOW_LISTS:                  0b0000100000000000,
    SHOW_USERS:                  0b0001000000000000,
    EDIT_SETTINGS:               0b0010000000000000,
    EDIT_RIGHTS:                 0b0100000000000000,
    EDIT_USERS:                  0b1000000000000000,
    ALL:                         0b1111111111111111,
};

export const ROOM_RIGHTS = { //when changing, look in main (db defaults)
    ADD_ITEM:                    0b0000001, //change or add articles
    REMOVE_ITEM:                 0b0000010, 
    VIEW_LIST_CAT_PROD:          0b0000100, //edit room intern listGroups and listItems
    CHANGE_META:                 0b0001000,
    OTA:                         0b0010000, //edit otas
    MANAGE_MEMBERS:              0b0100000,
    CHANGE_ADMIN:                0b1000000,
};
