import { selfTag } from "../sys/selfTag.js";

const WELL_KNOWN_PATH = "/.well-known/outbag/server";
const DEFAULT_PORT = "7223";

const fetchWellknown = async (uri: URL) => {
    uri = new URL(uri);
    let resp = await fetch(uri);
    let json = await resp.json();
    if (json.path == null || json.port == null) throw new Error("NotAValidWellKnown");
    return { host: uri.hostname, path: json.path as string, port: json.port as string };
};

export const outbagURLfromTag = async (tag: string) => {
    let uri: URL;
    try {
        uri = new URL("/", tag);
        uri.protocol = "https";
    } catch (_) {
        uri = new URL("https://" + tag);
    }
    uri.pathname = WELL_KNOWN_PATH;
    let isMain = uri.port == '';
    try {
        let { host, path, port } = await fetchWellknown(uri);
        return new outbagServer(isMain ? host : host + ":" + port, host, path, port);
    } catch (_) {
        if (isMain) {
            try {
                uri.port = DEFAULT_PORT;
                let { host, path, port } = await fetchWellknown(uri);
                return new outbagServer(isMain ? host : host + ":" + port, host, path, port);
            } catch (_) { }
        }
        throw new Error("InvalidOutbagServer");
    }
};

export class outbagServer {
    host: string;
    port: string;
    path: string;
    tag: string;
    constructor(tag: string, host: string, path: string, port: string) {
        this.host = host;
        this.port = port;
        if (!path.startsWith("/")) this.path = "/" + path;
        else this.path = path;
        this.tag = tag;
    }
    get httpsURL() {
        return `https://${this.host}:${this.port}${this.path}`;
    }
    get wsURL() {
        return `wss://${this.host}:${this.port}${this.path}`;
    }
};

export const checkSelfTag = (tag: string) => {
    return tag == selfTag.tag || tag == selfTag.host + ":" + selfTag.port;
}