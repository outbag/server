

export const act_error = {
    // common errors
    CLOSED: "closed", // Server is closed
    BRUTEFORCE: "bruteforce", // request cancled tue to brutforce suspiciot, try later
    NOT_FOUND: "notfound", // act does not exists
    WRONG_STATE: "wrongstate", // act can not be executed in current state
    DATA: "data", // send Data did not matche expected data structur of act
    RIGHT: "right", // your Server wide permissions do not allow you to do this
    SERVER: "server", // uncaught error in server
    RECURSION: "recursion", // not allowed due to suspected remote recursion, will only appear with miss configureation
    REMOTE: "remote", // error while remote request (like could not contact the remote server)
    CONNECTION: "connection", // the current connection type is not correct

    //client + admin errors
    CLIENT_NOT_EXISTS: "clientnotexists", // seems like your own account does not exists (client.ts acts)
    ACCOUNT_NOT_EXISTS: "accountnotexists", // referred account does not exists (admin / client trennen? )
    ACCOUNT_EXISTS: "accountexists", // referred account already exists
    ROOM_EXISTS: "roomexists", // the new room already exists
    ROOM_NOT_EXISTS: "roomnotexists", // the requested / referred to room does not exists (or does not exists for you)
    MEMBER_NOT_EXISTS: "membernotexists", // referred Member does not exists (allways not you)
    MEMBER_EXISTS: "memberexists", // referred Member already exists
    DUPLICATE: "duplicate", // you are already a member
    ROOM_LIMIT: "roomlimit", // you have exited your Room number limit
    ROOM_USER_LIMIT: "roomuserlimit", // this room is full
    ROOM_DATA_LIMIT: "roomdatalimit", // room is full
    NOT_ROOM_ADMIN: "notroomadmin", // your are not an Admin of this room, if it exists
    NOT_IN_ROOM: "notinroom", // your are not in this room
    OWNER: "owner", // you are the owner, so you are not allowed to do this

    CAT_NOT_EXISTS: "catnotexists", // referred category does not exists
    PROD_NOT_EXISTS: "prodnotexists", // referred product does not exists
    ITEM_NOT_EXISTS: "itemnotexists", // referred Item does not exists
    ADD_CAT: "addcat", // adding categorie did not work
    ADD_PROD: "addprod", // adding product did not work
    ADD_ITEM: "additem", // adding item did not work

    CONFIG: "config", // server is full, you need and ota to succeed
    OTA: "ota", // given ota is invalid
    AUTH: "auth", // provided auth informations are wrong
    SIGNATURE: "signature", // can not verify provided signature
    TOKEN: "token", // auth token is invallid
 
    //server-to-server only 
    SERVER_NOT_EXISTS: "servernotexists", // remote Server login can not require requsting server's informations
    SERVER_TOKEN: "serverToken", // wrong server token 
};