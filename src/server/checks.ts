import { and, le, remove } from "dblang";
import { addShutdownTask } from "nman";
import { accounts, db } from "../sys/db.js"
import { uts } from "../sys/tools.js";
import { oConf } from "../sys/config.js";

export function startChecks() {
    let i1 = setInterval(async () => {
        await remove(accounts)
            .where(and(
                accounts.deleted,
                le(accounts.deletedTime, uts() - oConf.get("Settings", "deletedAccountKeepSec"))
            )).query(db);
    }, 60000);

    addShutdownTask(() => clearInterval(i1), 500);
}