import { outbagServer } from "./outbagURL.js";
import { debug, log } from "../sys/log.js";
import { uts } from "../sys/tools.js";
import { getSettings, setSettings, SETTINGS } from "../sys/settings.js"
import { generateSigningKey } from "../sys/crypto.js"
import { oConf } from "../sys/config.js";
import nman, { addShutdownTask } from "nman";
import { insert } from "dblang";
import { db, servers } from "../sys/db.js";

export const startUpdateCert = async () => {
    const update = async () => {
        var exp = parseInt(await getSettings(SETTINGS.certExpires));
        if (uts() - 60 >= (exp || 0)) {
            log("serverCert", "generete new Cert");
            let { privateKey, publicKey } = await generateSigningKey();
            await setSettings(SETTINGS.publicKey, publicKey);
            await setSettings(SETTINGS.privateKey, privateKey);
            await setSettings(SETTINGS.certExpires, String(uts() + oConf.get("System", "CertLiveSec")));
        }
    };
    let intervalId = setInterval(update, 1000 * 60);
    await update();

    nman.addShutdownTask(() => {
        clearInterval(intervalId);
    }, 100);
}

const deleteafter = 60 * 60;

const certList: { [key: string]: { exp: number, cert: string } } = {};

var certListCleaner = setInterval(async () => {
    var utst = uts();
    let keys = Object.keys(certList);
    for (var i = 0; i < keys.length; i++) {
        if (utst >= certList[keys[i]].exp) {
            debug("Certificate List", "remove Cert: ", keys[i]);
            delete certList[keys[i]];
        }
    }
}, 1000 * 60);
addShutdownTask(() => clearInterval(certListCleaner), 5000);

const updateCert = async (server: outbagServer) => {
    try {
        let resp = await fetch(server.httpsURL + "api/server/publicKey")
        let json = await resp.json();
        let { publicKey, expires } = json;
        certList[server.tag] = { exp: Math.min(expires, uts() + deleteafter), cert: publicKey };
        try {
            await insert(servers.tag)
                .add(server.tag)
                .query(db);
        } catch (error) { }
        return true;
    } catch (error) {
        return false;
    }
};

export const getRemote = async (server: outbagServer) => {
    if (certList[server.tag] == null || certList[server.tag].exp >= uts()) await updateCert(server);
    if (certList[server.tag] != null) return certList[server.tag].cert;
    else throw new Error("Cert Error");
};