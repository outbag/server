

export const LISTENER_TYPE = {
    ROOMINFO: 0,
    ITEMS: 1,
    CATEGORIES: 2,
    PRODUCTS: 3,
}

export const LISTENER_ACTION = {
    ADD: 0,
    CHANGE: 1,
    DELETE: 2,
}