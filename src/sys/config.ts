import juml from "juml";

const conf_struct = {
    System: {
        PORT: { type: "number", default: 7223, env: "OUTBAG_PORT", comment: "The Server will listen on this Port!" },
        PORTexposed: { type: "number", default: 7223, env: "OUTBAG_EXPOSED_PORT" },
        PATHexposed: { type: "string", default: "/", env: "OUTBAG_EXPOSED_PATH" },
        URL: { type: "string", default: "localhost", env: "OUTBAG_HOST" },
        CertLiveSec: { type: "number", default: 60 * 60 * 24 * 30, env: "OUTBAG_CERT_LIVE_SEC" },
    },
    ssl: {
        enable: { type: "boolean", default: false, env: "OUTBAG_SSL_ENABLED" },
        privkey: { type: "string", default: "privkey.pem", env: "OUTBAG_SSL_PRIVATE_KEY" },
        cert: { type: "string", default: "cert.pem", env: "OUTBAG_SSL_CERT" },
        chain: { type: "string", default: "chain.pem", env: "OUTBAG_SSL_CHAIN" }
    },
    Database: {
        host: { type: "string", default: "localhost", env: "OUTBAG_MYSQL_HOST" },
        port: { type: "number", default: 3306, env: "OUTBAG_MYSQL_PORT" },
        user: { type: "string", default: "admin", env: "OUTBAG_MYSQL_USER" },
        password: { type: "string", default: "", env: "OUTBAG_MYSQL_PASSWORD" },
        database: { type: "string", default: "outbag", env: "OUTBAG_MYSQL_DATABASE" }
    },
    Settings: {
        maxUsers: { type: "number", default: 0, env: "OUTBAG_MAX_USERS" },//Infinity = -1
        defaultMaxRooms: { type: "number", default: 3, env: "OUTBAG_DEFAULT_MAX_ROOMS" },//Infinity = -1
        defaultMaxRoomSize: { type: "number", default: 10000, env: "OUTBAG_DEFAULT_MAX_ROOMS_SIZE" },//Infinity = -1
        defaultMaxUsersPerRoom: { type: "number", default: 5, env: "OUTBAG_DEFAULT_MAX_USERS_PER_ROOM" },//Infinity = -1
        defaultViewable: { type: "boolean", default: false, env: "OUTBAG_DEFAULT_VIEWABLE" },
        deletedAccountKeepSec: { type: "number", default: 60 * 60 * 24 * 20, env: "OUTBAG_DEL_ACCOUNT_KEEP_SEC" },
    }
};

export const oConf = new juml(conf_struct);