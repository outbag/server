import { addShutdownTask } from "nman";
import { log } from "./log.js"
import { uts } from "./tools.js";
import express from "express";

const timeout = 10;
const deleteafter = 600;
const maxSus = 100;

var bruteforcedata: { [key: string]: { n: number, t: number } } = {};

export const addBruteforcePotantial = (ip: string) => {
    if (bruteforcedata[ip] == null) {
        bruteforcedata[ip] = { n: 0, t: uts() };
        log("Bruteforce Protection", "add ip: ", ip);
    }
    bruteforcedata[ip].n++;
    bruteforcedata[ip].t = uts();
    if (bruteforcedata[ip].n > maxSus) {
        log("Bruteforce Protection", "blocking ip: ", ip);
    }
};

export const bruteforcecheck = (ip: string) => {
    return (bruteforcedata[ip]?.n || 0) <= maxSus || uts() - (bruteforcedata[ip]?.t || uts()) > timeout;
};

var bruteforcedatacleaner = setInterval(async () => {
    var utst = uts();
    let keys = Object.keys(bruteforcedata);
    for (var i = 0; i < keys.length; i++) {
        if (utst - bruteforcedata[keys[i]].t > deleteafter) {
            log("Bruteforce Protection", "remove ip: ", keys[i]);
            delete bruteforcedata[keys[i]];
        }
    }
}, 1000 * 60);

addShutdownTask(() => clearInterval(bruteforcedatacleaner), 5000);

export interface suspectRequest extends express.Request {
    suspect?: () => void
};

export default (req: suspectRequest, res: express.Response, next: express.NextFunction) => {
    if (!bruteforcecheck(req.ip)) return void res.status(400).send("bruteforce");
    req.suspect = () => {
        addBruteforcePotantial(req.ip);
    };
    next();
};