import { outbagServer, outbagURLfromTag } from "../server/outbagURL.js";
import { oConf } from "./config.js"
import { debug } from "./log.js";

export let selfTag: outbagServer = new outbagServer(
    "localhost",
    oConf.get("System", "URL") + "",
    oConf.get("System", "PATHexposed") + "",
    oConf.get("System", "PORTexposed") + "",
);

export const generateTag = async () => {
    try {
        selfTag = new outbagServer(
            "localhost",
            oConf.get("System", "URL") + "",
            oConf.get("System", "PATHexposed") + "",
            oConf.get("System", "PORTexposed") + "",
        );
        let initselfTag = selfTag;
        let mainServerHost: outbagServer | null = null;
        try {
            mainServerHost = await outbagURLfromTag(oConf.get("System", "URL"));
        } catch (error) { }
        let serverHostPort = await outbagURLfromTag(
            oConf.get("System", "URL") + ":" + oConf.get("System", "PORTexposed"));
        if (mainServerHost == null || mainServerHost.port != serverHostPort.port) {
            selfTag = serverHostPort;
        } else selfTag = mainServerHost;
        debug("Outbag", "Self Server Tag is:", selfTag.tag);
        if (initselfTag.httpsURL != selfTag.httpsURL) {
            debug("Outbag", "Not matching Server host, port, path and expected server link.");
            return false;
        }
        return true;
    } catch (error) {
        return false;
    }
};