import { eq, exists, insert, not, select, update } from "dblang";
import { db, settings } from "./db.js";

export const SETTINGS = {
    publicKey: "publicKey",
    privateKey: "privateKey",
    certExpires: "certExpires"
};

export const getSettings = async (type: string) => {
    let query = await select([settings.data], settings)
        .where(eq(settings.type, type))
        .query(db);
    if (!query.length) {
        return false;
    }
    return query[0][settings.data];
};

export const setSettings = async (type: string, data: string) => {
    try {
        let resp = await insert(settings.type, settings.data)
            .add(type, data)
            .query(db);
        if (resp.affectedRows == 0) throw new Error("insertion fail");
    } catch (error) {
        await update(settings)
            .set(settings.data, data)
            .where(eq(settings.type, type))
            .query(db);
    }

};