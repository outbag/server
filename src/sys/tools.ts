import crypto from "crypto";

export const uts = () => {
    return Math.floor(new Date().getTime() / 1000);
}

const key64 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";

export const get64 = (l: number) => {
    var out = "";
    var val = crypto.webcrypto.getRandomValues(new Uint8Array(l));

    for (var i = 0; i < l; i++) {
        out += key64[val[i] % key64.length];
    }
    return out;
};

export const wait = (ms: number) => {
    return new Promise((res)=>{
        setTimeout(res,ms);
    })
}