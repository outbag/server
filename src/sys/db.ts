import { BIGINT, BOOL, DB, INT, onAction, SMALLINT, TEXT, TINYINT, uniqueKey, VARCHAR } from "dblang"
import { oConf } from "./config.js"
import { PERMISSIONS } from "../server/permissions.js"
import nMan from "nman";
import { log, error, debug } from "./log.js";

export const db = new DB();

export const connectToDB = async () => {
    db.connect({
        host: oConf.get("Database", "host"),
        port: oConf.get("Database", "port"),
        user: oConf.get("Database", "user"),
        password: oConf.get("Database", "password"),
        database: oConf.get("Database", "database"),
    });
    accounts.maxRooms.ops.default = oConf.get("Settings", "defaultMaxRooms");
    accounts.maxRoomSize.ops.default = oConf.get("Settings", "defaultMaxRoomSize");
    accounts.maxUsersPerRoom.ops.default = oConf.get("Settings", "defaultMaxUsersPerRoom");
    accounts.viewable.ops.default = oConf.get("Settings", "defaultViewable");
    try {
        await db.sync(true);
        log("Database", "Connected to Database!");
    } catch (e) {
        error("Database", "Error while connecting to Database!");
        debug("Database", "Error:", e);
        throw e;
    }
};

export const connectToDBCredentials = async (host: string, port: number, user: string, password: string, database: string) => {
    db.connect({
        host, port, user, password, database,
    });
    accounts.maxRooms.ops.default = oConf.get("Settings", "defaultMaxRooms");
    accounts.maxRoomSize.ops.default = oConf.get("Settings", "defaultMaxRoomSize");
    accounts.maxUsersPerRoom.ops.default = oConf.get("Settings", "defaultMaxUsersPerRoom");
    accounts.viewable.ops.default = oConf.get("Settings", "defaultViewable");
    try {
        await db.sync(true);
    } catch (e) {
        throw e;
    }
};

nMan.addShutdownTask(db.close, 3000, 10);

export const accounts = db.newTable("accounts");
accounts.addAttributes({
    accID: { type: INT, primaryKey: true, autoIncrement: true },
    name: { type: VARCHAR(100), unique: true, notNull: true },

    rights: { type: INT, default: PERMISSIONS.DEFAULT },

    accountKeySalt: { type: VARCHAR(64) },
    accountKey: { type: VARCHAR(64) },

    viewable: { type: BOOL, default: false },

    deleted: { type: BOOL, default: false },
    deletedTime: { type: BIGINT, default: 0 },

    maxRooms: { type: INT },
    maxRoomSize: { type: INT },
    maxUsersPerRoom: { type: INT },
});

export const remoteRooms = db.newTable("remoteRooms");
remoteRooms.addAttributes({
    accID: {
        type: INT,
        primaryKey: true,
        foreignKey: {
            link: accounts.accID,
            onUpdate: onAction.cascade,
            onDelete: onAction.cascade
        }
    },
    server: {
        type: VARCHAR(256),
        primaryKey: true,
    },
    room: {
        type: VARCHAR(256),
        primaryKey: true,
    },
    confirmed: {
        type: BOOL,
        default: false
    }
});

export const settings = db.newTable("settings");
settings.addAttributes({
    type: { type: VARCHAR(256), primaryKey: true },
    data: { type: TEXT },
})

export const servers = db.newTable("servers");
servers.addAttributes({
    tag: { type: VARCHAR(256), primaryKey: true },
    //publicKey: { type: TEXT },
    //expires: { type: BIGINT },
    token: { type: TEXT, notNull: false },
});

export const signupOTA = db.newTable("signupOTA");
signupOTA.addAttributes({
    token: { type: VARCHAR(128), primaryKey: true },
    name: { type: VARCHAR(256) },
    expires: { type: BIGINT },
    usesLeft: { type: INT }
});

export const rooms = db.newTable("rooms");
rooms.addAttributes({
    roomID: { type: INT, primaryKey: true, autoIncrement: true },
    name: { type: VARCHAR(256), unique: true },
    owner: {
        type: INT,
        foreignKey: {
            link: accounts.accID,
            onDelete: onAction.cascade,
            onUpdate: onAction.cascade
        }
    },
    rights: { type: INT, default: 0b11111 },
    visibility: { type: SMALLINT, default: 0 },
    title: { type: TEXT, default: "" },
    description: { type: TEXT, default: "" },
    icon: { type: TEXT, default: "" }
});

export const roomMembers = db.newTable("roomMembers");
roomMembers.addAttributes({
    roomMemberID: { type: INT, primaryKey: true, autoIncrement: true },
    roomID: {
        type: INT,
        foreignKey: {
            link: rooms.roomID,
            onDelete: onAction.cascade,
            onUpdate: onAction.cascade
        }
    },
    name: { type: VARCHAR(256) },
    server: { type: VARCHAR(256) },
    admin: { type: BOOL, default: false },
    confirmed: { type: BOOL, default: false }
});
roomMembers.addConstraint(uniqueKey([
    roomMembers.roomID,
    roomMembers.name,
    roomMembers.server
]));

export const roomOTAs = db.newTable("roomOTAs");
roomOTAs.addAttributes({
    roomID: {
        type: INT,
        primaryKey: true,
        foreignKey: {
            link: rooms.roomID,
            onDelete: onAction.cascade,
            onUpdate: onAction.cascade
        }
    },
    name: { type: VARCHAR(256) },
    token: {
        type: VARCHAR(256),
        primaryKey: true
    },
    expires: { type: BIGINT },
    usesLeft: { type: INT },
});

export const listCategories = db.newTable("listCategories");
listCategories.addAttributes({
    listCatID: { type: INT, primaryKey: true, autoIncrement: true },
    roomID: {
        type: INT,
        foreignKey: {
            link: rooms.roomID,
            onDelete: onAction.cascade,
            onUpdate: onAction.cascade
        }
    },
    title: { type: VARCHAR(256) },
    weight: { type: INT },
    color: { type: VARCHAR(32) }
});

export const listProducts = db.newTable("listProducts");
listProducts.addAttributes({
    listProdID: { type: INT, primaryKey: true, autoIncrement: true },
    roomID: {
        type: INT,
        foreignKey: {
            link: rooms.roomID,
            onDelete: onAction.cascade,
            onUpdate: onAction.cascade
        }
    },
    title: { type: VARCHAR(256) },
    description: { type: VARCHAR(4096) },
    category: {
        type: INT,
        foreignKey: {
            link: listCategories.listCatID,
            onDelete: onAction.setNull,
            onUpdate: onAction.cascade
        }
    },
    defUnit: { type: SMALLINT },
    defValue: { type: VARCHAR(256) },
    ean: { type: VARCHAR(64), notNull: false }
});
listProducts.addAttribute("parent", INT, {
    foreignKey: {
        link: listProducts.listProdID,
        onDelete: onAction.setNull,
        onUpdate: onAction.cascade
    }
});

export const listItems = db.newTable("listItems");
listItems.addAttributes({
    listItemID: { type: BIGINT, primaryKey: true, autoIncrement: true },
    roomID: {
        type: INT,
        foreignKey: {
            link: rooms.roomID,
            onDelete: onAction.cascade,
            onUpdate: onAction.cascade
        }
    },
    state: { type: SMALLINT, default: 0 },
    lastStateTime: { type: BIGINT, default: 0 },
    title: { type: TEXT },
    description: { type: TEXT },
    category: {
        type: INT,
        foreignKey: {
            link: listCategories.listCatID,
            onDelete: onAction.setNull,
            onUpdate: onAction.cascade
        }
    },
    unit: { type: SMALLINT },
    value: { type: TEXT },
    link: {
        type: INT,
        foreignKey: {
            link: listProducts.listProdID,
            onDelete: onAction.setNull,
            onUpdate: onAction.cascade
        }
    }
});
