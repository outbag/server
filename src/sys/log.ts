import { addShutdownTask } from "nman";

export const logList: ([number, "debug" | "log" | "warn" | "error", string, string, any[]])[] = [
    //[id, "type", "Date", "Titel", ...data]
];
let logID = 0;

export const debug = (name: string, ...args: any[]) => {
    if (!global.debug) return;
    args = consorArgs(args);
    let dateString = (new Date()).toLocaleString();
    console.log(
        "\x1b[33m%s\x1b[0m" +
        "\x1b[1m\x1b[32m%s\x1b[0m",
        dateString,
        ` [${name}]:`,
        ...args
    );
    if (global.provideLog != 0) logList.push([logID++, "debug", dateString, name, args]);
};

export const log = (name: string, ...args: any[]) => {
    args = consorArgs(args);
    let dateString = (new Date()).toLocaleString();
    console.log(
        "\x1b[33m%s\x1b[0m" +
        "\x1b[1m\x1b[36m%s\x1b[0m",
        dateString,
        ` [${name}]:`,
        ...args
    );
    if (global.provideLog != 0) logList.push([logID++, "log", dateString, name, args]);
};

export const warn = (name: string, ...args: any[]) => {
    args = consorArgs(args);
    let dateString = (new Date()).toLocaleString();
    console.warn(
        "\x1b[33m%s\x1b[0m" +
        "\x1b[1m\x1b[48;5;208m%s\x1b[0m\x1b[48;5;208m",
        dateString + " ",
        `[${name}]:`,
        ...args,
        "\x1b[0m"
    );
    if (global.provideLog != 0) logList.push([logID++, "warn", dateString, name, args]);
};

export const error = (name: string, ...args: any[]) => {
    let dateString = (new Date()).toLocaleString();
    console.error(
        "\x1b[33m%s\x1b[0m" +
        "\x1b[1m\x1b[41m%s\x1b[0m\x1b[41m",
        dateString + " ",
        `[${name}]:`,
        ...args,
        "\x1b[0m"
    );
    if (global.provideLog != 0) logList.push([logID++, "error", dateString, name, args]);
};

let clearLogID = setInterval(() => {
    if (global.provideLog >= -1)
        logList.splice(0, logList.length - global.provideLog)
}, 60000);
addShutdownTask(() => clearInterval(clearLogID));

const consorArgs = (args: any[]) => {
    let out = [];
    for (let i = 0; i < args.length; i++) {
        const arg = args[i];
        out[i] = censorLogArg(arg);
    }
    return out;
}
const censorLogArg = (arg: any) => {
    if (typeof arg != "object" || arg == null || arg instanceof Error) return arg;
    let out: any = Array.isArray(arg) ? [] : {};
    for (let key in arg) {
        if ((["accountKey", "sign", "publicKey", "token", "OTA"]).includes(key)) out[key] = new Array(Math.round(Math.random() * 25 + 25)).fill("*").join("");
        else out[key] = censorLogArg(arg[key]);
    }
    return out;
}