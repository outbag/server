import fs from "fs";
import { shutdown } from "nman";
import prompts from "prompts";
import { oConf } from "../sys/config.js";
import { connectToDB, connectToDBCredentials, db } from "../sys/db.js";
import { log, error, warn } from "../sys/log.js";

const isFile = async (path: string) => {
    try {
        if (path.startsWith("http")) {
            let req = await fetch(path);
            return req.status == 200;
        } else {
            await fs.promises.access(path, fs.constants.F_OK);
            let state = await fs.promises.stat(path);
            return state.isFile();
        }
    } catch (error) {
        return false;
    }
    return true;
}

const loading = () => {
    //var P = "⣾⣽⣻⢿⡿⣟⣯⣷";
    let P = "⠋⠙⠹⠸⠼⠴⠦⠧⠇⠏";
    var x = 0;
    let intID = setInterval(function () {
        process.stdout.write("\r\x1b[1m" + P[(x++) % P.length] + "   ");
    }, 150);
    return () => {
        process.stdout.write("\r\x1b[0m");
        clearInterval(intID)
    };
};

const system = async () => {
    log("Setup", "Setting up generell System Settings:");
    const resp = await prompts([
        {
            type: "number",
            name: "port",
            message: "Please enter the Port the Server should listen on!",
            initial: oConf.get("System", "PORT")
        }, {
            type: "number",
            name: "portexposed",
            message: "Please enter the Port on which the server is exposed!",
            initial: oConf.get("System", "PORTexposed")
        }, {
            type: "text",
            name: "pathexposed",
            message: "Please enter the Path on which the server is exposed!",
            initial: oConf.get("System", "PATHexposed")
        }, {
            type: "text",
            name: "url",
            message: "Please enter the host name of the server!",
            initial: oConf.get("System", "URL")
        }, {
            type: "number",
            name: "certlivesec",
            message: "Please enter the number of days  after the Server Certificate should be expire!",
            initial: oConf.get("System", "CertLiveSec") / 60 / 60 / 24
        }
    ]);
    if (
        resp.port == null
        || resp.portexposed == null
        || resp.pathexposed == null
        || resp.url == null
        || resp.certlivesec == null
    ) {
        warn("Setup", "You have to provide all informations!");
        return false;
    };

    const resp2 = await prompts({
        type: "confirm",
        name: "correct",
        message: "Are the Details correct?",
        initial: true
    });

    if (!resp2.correct) {
        return false;
    }

    oConf.set("System", "PORT", resp.port);
    oConf.set("System", "PORTexposed", resp.portexposed);
    oConf.set("System", "PATHexposed", resp.pathexposed);
    oConf.set("System", "URL", resp.url);
    oConf.set("System", "CertLiveSec", Math.round(resp.certlivesec * 24 * 60 * 60));
    oConf.save();
    return true;
}

const ssl = async () => {
    log("Setup", "Setting up ssl server:");
    const respActive = await prompts({
        type: "confirm",
        name: "ssl",
        message: "Activate SSL? (If deactivated Server will run in test mode!)",
        initial: true
    });
    if (respActive.ssl == null) return false;
    if (respActive.ssl == false) {
        oConf.set("ssl", "enable", false);
    }
    const resp = await prompts([
        {
            type: "text",
            name: "privkey",
            message: "Please enter path or link to privkey.pem",
            validate: async (path: string) => await isFile(path) ? true : "Please enter a path or a link to a readable File!",
            initial: oConf.get("ssl", "privkey")
        }, {
            type: "text",
            name: "cert",
            message: "Please enter path or link to cert.pem",
            validate: async (path: string) => await isFile(path) ? true : "Please enter a path or a link to a readable File!",
            initial: oConf.get("ssl", "cert")
        }, {
            type: "text",
            name: "chain",
            message: "Please enter path or link to chain.pem",
            validate: async (path: string) => await isFile(path) ? true : "Please enter a path or a link to a readable File!",
            initial: oConf.get("ssl", "chain")
        }
    ]);

    if (
        resp.privkey == null
        || resp.cert == null
        || resp.chain == null
    ) {
        warn("Setup", "You have to provide all informations!");
        return false;
    };

    const resp2 = await prompts({
        type: "confirm",
        name: "correct",
        message: "Are the Details correct?",
        initial: true
    });

    if (!resp2.correct) {
        return false;
    }

    oConf.set("ssl", "enable", true);
    oConf.set("ssl", "privkey", resp.privkey);
    oConf.set("ssl", "cert", resp.cert);
    oConf.set("ssl", "chain", resp.chain);
    oConf.save();
    return true;
}

const database = async () => {
    log("Setup", "Setting up Database:");
    const resp = await prompts([
        {
            type: "text",
            name: "host",
            message: "Please enter database host!",
            initial: oConf.get("Database", "host")
        }, {
            type: "number",
            name: "port",
            message: "Please enter database port!",
            initial: oConf.get("Database", "port")
        }, {
            type: "text",
            name: "user",
            message: "Please enter the database username!",
            initial: oConf.get("Database", "user")
        }, {
            type: "password",
            name: "password",
            message: "Please enter the database password!",
            initial: oConf.get("Database", "password")
        }, {
            type: "text",
            name: "database",
            message: "Please enter the Outbag database Name!",
            initial: oConf.get("Database", "database")
        }
    ]);
    if (resp.host == null || resp.port == null || resp.user == null || resp.password == null || resp.database == null) {
        warn("Setup", "You have to provide all informations!");
        return false;
    }

    const resp2 = await prompts({
        type: "confirm",
        name: "correct",
        message: "Are the Details correct?",
        initial: true
    });

    if (!resp2.correct) {
        return false;
    }

    log("Setup", "Trying to connect to database: ");
    let cancle = loading();
    try {
        await connectToDBCredentials(resp.host, resp.port, resp.user, resp.password, resp.database);
        cancle();
        log("Setup", "Successful connected to Database!");
        oConf.set("Database", "host", resp.host);
        oConf.set("Database", "port", resp.port);
        oConf.set("Database", "user", resp.user);
        oConf.set("Database", "password", resp.password);
        oConf.set("Database", "database", resp.database);
        oConf.save();
        await db.close();
        return true;
    } catch (e) {
        cancle();
        error("Setup", "Error while connecting to Database!");
        return false;
    }
}

const outbag = async () => {
    log("Setup", "Setting up generell Outbag Settings:");
    const resp = await prompts([
        {
            type: "number",
            name: "maxUsers",
            message: "Please enter the maximum amount of server users!",
            initial: oConf.get("Settings", "maxUsers"),
            validate: i => i >= -1
        }, {
            type: "number",
            name: "defaultMaxRooms",
            message: "Please enter the default maximum number of rooms!",
            initial: oConf.get("Settings", "defaultMaxRooms"),
            validate: i => i >= -1
        }, {
            type: "number",
            name: "defaultMaxRoomSize",
            message: "Please enter the default maximum amount of Elements in one Room!",
            initial: oConf.get("Settings", "defaultMaxRoomSize"),
            validate: i => i >= -1
        }, {
            type: "number",
            name: "defaultMaxUsersPerRoom",
            message: "Please enter default maximum number of Users per Room!",
            initial: oConf.get("Settings", "defaultMaxUsersPerRoom"),
            validate: i => i >= -1
        }, {
            type: "confirm",
            name: "defaultViewable",
            message: "Should outbag accounts be viewable by default?",
            initial: oConf.get("Settings", "defaultViewable"),
        }, {
            type: "number",
            name: "deletedAccountKeepSec",
            message: "Please enter the number of days a deleted Account is being save until final deletion!",
            initial: oConf.get("Settings", "deletedAccountKeepSec") / 60 / 60 / 24
        }
    ]);
    if (
        resp.maxUsers == null
        || resp.defaultMaxRooms == null
        || resp.defaultMaxRoomSize == null
        || resp.defaultMaxUsersPerRoom == null
        || resp.defaultViewable == null
        || resp.deletedAccountKeepSec == null
    ) {
        warn("Setup", "You have to provide all informations!");
        return false;
    }
    const resp2 = await prompts({
        type: "confirm",
        name: "correct",
        message: "Are the Details correct?",
        initial: true
    });

    if (!resp2.correct) {
        return false;
    }

    oConf.set("Settings", "maxUsers", resp.maxUsers);
    oConf.set("Settings", "defaultMaxRooms", resp.defaultMaxRooms);
    oConf.set("Settings", "defaultMaxRoomSize", resp.defaultMaxRoomSize);
    oConf.set("Settings", "defaultMaxUsersPerRoom", resp.defaultMaxUsersPerRoom);
    oConf.set("Settings", "defaultViewable", resp.defaultViewable);
    oConf.set("Settings", "deletedAccountKeepSec", Math.round(resp.deletedAccountKeepSec * 60 * 60 * 24));
    oConf.save();

    return true;
}

const setupFunc = async (func: () => Promise<boolean>, name: string) => {
    let dbConn = await func();
    while (!dbConn) {
        console.log();
        const resp = await prompts({
            type: "confirm",
            name: "again",
            message: "Try to setup '" + name + "' again?",
            initial: false
        });
        if (!resp.again) break;
        dbConn = await func();
    }
};

export const partiellSetup = async () => {
    while (true) {
        log("Setup", "Selection:");
        const resp = await prompts({
            type: 'select',
            name: 'value',
            message: 'Which Settings do you want to change?',
            choices: [
                { title: 'System', description: 'Setup generell System Settings.' },
                { title: 'SSL', description: 'Setup ssl details.' },
                { title: 'Database', description: 'Setup Database connection.' },
                { title: 'Outbag', description: 'Change generell Outbag Settings.' },
            ],
            initial: 0
        });
        if (resp.value == 0) await setupFunc(system, 'System');
        else if (resp.value == 1) await setupFunc(ssl, 'SSL');
        else if (resp.value == 2) await setupFunc(database, 'Database');
        else if (resp.value == 3) await setupFunc(outbag, 'Outbag');
        else break;
    }
    await shutdown();
}

export const fullSetup = async () => {
    log("Setup", "Starting full setup script:");
    await setupFunc(system, 'System');
    console.log();
    await setupFunc(ssl, 'SSL');
    console.log();
    await setupFunc(database, 'Database');
    console.log();
    await setupFunc(outbag, 'Outbag');
    log("Setup", "Finished setup script!");
    await shutdown();
}