import WebSocket from 'ws';
import { uts, wait } from '../dist/sys/tools.js';
import post from "./tests/post.js"
import { spawn } from "child_process";
import { oConf } from "../dist/sys/config.js"
import mariadb from "mariadb";

let inCI = process.argv.includes("ci");

oConf.connect(inCI ? 'test.juml' : 'testLocal.juml');

const connection = await mariadb.createConnection({
    host: oConf.get("Database", "host"),
    port: oConf.get("Database", "port"),
    user: oConf.get("Database", "user"),
    password: oConf.get("Database", "password"),
    database: oConf.get("Database", "database"),
    multipleStatements: true
});
await wait(100);

let req = await connection.query("SELECT table_name as n FROM information_schema.tables WHERE table_schema = ?;", [oConf.get("Database", "database")]);
let sql = [...req].map(d => "DROP TABLE IF EXISTS `" + d.n + "`;").join("");
if (sql.length > 5) await connection.query("SET FOREIGN_KEY_CHECKS = 0;" + sql + "SET FOREIGN_KEY_CHECKS = 1;", []);
connection.close();

function shallowEqual(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length < keys2.length) {
        return false;
    }
    for (let key of keys1) {
        if (typeof object1[key] == "object" && object1[key] != null) {
            if(typeof object2[key] != "object") return false;
            if(!shallowEqual(object1[key], object2[key])) return false;
        } else if (object1[key] != null && object1[key] != object2[key]) {
            return false;
        }
    }
    return true;
}


async function postTester(url) {
    async function test(header, act, data, expState, expData) {
        console.log("Testing Act:", act, "with Data:", data);
        let fetchResp = await fetch(url + "api/" + act, {
            method: "POST",
            headers: Object.assign({
                "Content-Type": "application/json"
            }, header),
            body: JSON.stringify({ data })
        });

        let resp = {
            state: fetchResp.status == 200 ? "ok" : "error",
            data: (await fetchResp.json()).data
        }

        if (resp.state != expState) {
            console.error(`Expected state: '${expState}', but got: '${resp.state}'`);
            kill();
            process.exit(1);
        }
        if (typeof expData == "object" && expData != null) {
            if (!shallowEqual(expData, resp.data)) {
                console.error(`Expected data: '${JSON.stringify(expData, null, 2)}', but got: '${JSON.stringify(resp.data, null, 2)}'`);
                kill();
                process.exit(1);
            }
        } else if (expData != null) {
            if (expData != resp.data) {
                console.error(`Expected data: '${expData}', but got: '${resp.data}'`);
                kill();
                process.exit(1);
            }
        }
        return resp.data;
    }

    for (let i = 0; i < post.length; i++) {
        const currTest = post[i];
        console.log(`Testing '${currTest[0]}':`);
        await wait(100);
        let resp = true;
        try {
            resp = await currTest[1](test);
        } catch (error) {
            console.error("Test Error: ", error);
            kill();
            process.exit(1);
        }
        if (resp === false) {
            console.log("Test respond with error indication!");
            kill();
            process.exit(1);
        }
    }
}


const ls = spawn('node', ['.', '-c', inCI ? 'test.juml' : 'testLocal.juml', '-d']);

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

ls.stdout.on('data', (data) => {
    process.stdout.write(data);
    if (data.includes("Listening...")) test();
});

ls.stderr.on('data', (data) => {
    console.error(`stderr: ${data}`);
});

ls.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    process.exit(code);
});

function kill() {
    ls.kill('SIGINT');
}

let startet = false;

async function test() {
    if (startet) return;
    startet = true;
    console.log("Start testing POST");
    await postTester("https://localhost:7224/");
    kill();
    process.exit(0);
}