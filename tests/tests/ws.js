import { generateSigningKey, sign } from '../../dist/sys/crypto.js';
import { PERMISSIONS } from '../../dist/server/permissions.js';
import { act_error } from '../../dist/server/errors.js';

let name1 = "testUser1";
let name2 = "testUser2";
let name3 = "testUser3";
let accountKey = "123456789";

let room1 = "r1";
let room2 = "r2";
let room3 = "r3";

let { privateKey, publicKey } = await generateSigningKey();

const list = [
    ["signup", async (handler, req, newHandler) => {
        await req(handler, "signup", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        await req(handler, "signup", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.WRONG_STATE);
        await req(await newHandler(), "signup", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.ACCOUNT_EXISTS);
        await req(await newHandler(), "signup", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        await req(await newHandler(), "signup", {
            name: name3,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.CONFIG);
    }], ["remote", async (handler, req, newHandler) => {
        await req(handler, "signin", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        let signature = (await req(handler, "createSignature", {
            publicKey
        }, "ok", null)).sign;
        let falseSignature = await sign("lol", privateKey);
        let h2 = await newHandler();
        await req(h2, "remote1", {
            name: name1,
            server: "localhost:7223",
            publicKey,
            sign: signature
        }, "error", act_error.SIGNATURE);
        await req(h2, "remote1", {
            name: name1,
            server: "localhost:7224",
            publicKey,
            sign: falseSignature
        }, "error", act_error.SIGNATURE);
        let challenge = (await req(h2, "remote1", {
            name: name1,
            server: "localhost:7224",
            publicKey,
            sign: signature
        }, "ok", null)).challenge;
        await req(h2, "remote1", {
            name: name1,
            server: "localhost:7224",
            publicKey,
            sign: signature
        }, "error", act_error.WRONG_STATE);
        await req(h2, "remote2", {
            sign: await sign(challenge + "lol", privateKey)
        }, "error", act_error.SIGNATURE);
        await req(h2, "remote2", {
            sign: await sign(challenge, privateKey)
        }, "ok", "");
    }], ["account", async (handler, req, newHandler) => {
        await req(handler, "signin", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        await req(handler, "getMyAccount", {}, "ok", {
            rights: PERMISSIONS.ALL,
            name: name1,
            viewable: true,
            maxRooms: 2,
            maxRoomSize: 10,
            maxUsersPerRoom: 2,
        });
        let h2 = await newHandler();
        await req(h2, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        await req(h2, "getMyAccount", {}, "ok", {
            rights: PERMISSIONS.DEFAULT,
            name: name2,
            viewable: true,
            maxRooms: 2,
            maxRoomSize: 10,
            maxUsersPerRoom: 2,
        });
    }], ["change account", async (handler, req, newHandler) => {
        await req(handler, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        await req(handler, "changePassword", {
            accountKey: accountKey + "lol"
        }, "ok", "");
        let h2 = await newHandler();
        await req(h2, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.AUTH);
        await req(h2, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey: accountKey + "lol"
        }, "ok", "");
        await req(h2, "deleteAccount", {}, "ok", "");

        let h3 = await newHandler();
        await req(h3, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey: accountKey + "lol"
        }, "error", act_error.AUTH);
    }], ["room Owner", async (handler, req, newHandler) => {
        await req(handler, "signin", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "ok", "");
        await req(handler, "createRoom", {
            room: room1,
            server: "localhost:7224",
            title: "Test Room 1",
            description: "some desc",
            visibility: 0,
            icon: "shopping"
        }, "ok", "");
        await req(handler, "createRoom", {
            room: room1,
            server: "localhost:7224",
            title: "Test Room 1",
            description: "some desc",
            visibility: 0,
            icon: "shopping"
        }, "error", act_error.ROOM_EXISTS);

        await req(handler, "createRoom", {
            room: room2,
            server: "localhost:7224",
            title: "Test Room 2",
            description: "some desc 2",
            visibility: 1,
            icon: ""
        }, "ok", "");

        await req(handler, "createRoom", {
            room: room3,
            server: "localhost:7224",
            title: "Test Room 2",
            description: "some desc 2",
            visibility: 1,
            icon: ""
        }, "error", act_error.ROOM_LIMIT);

        await req(handler, "listRooms", {}, "ok", [
            {
                name: room1,
                server: "localhost:7224",
                owner: name1,
                rights: 0b11111,
                title: "Test Room 1",
                description: "some desc",
                visibility: 0,
                icon: "shopping",
                debug: true,
                confirmed: true
            }, {
                name: room2,
                server: "localhost:7224",
                owner: name1,
                rights: 0b11111,
                title: "Test Room 2",
                description: "some desc 2",
                visibility: 1,
                icon: "",
                debug: true,
                confirmed: true
            }
        ]);
        await req(handler, "deleteRoom", {
            room: room2 + "lol",
            server: "localhost:7224"
        }, "error", act_error.ROOM_NOT_EXISTS);
        await req(handler, "deleteRoom", {
            room: room2,
            server: "localhost:7224"
        }, "ok", "");
        await req(handler, "deleteRoom", {
            room: room2,
            server: "localhost:7224"
        }, "error", act_error.ROOM_NOT_EXISTS);
        await req(handler, "listRooms", {}, "ok", [
            {
                name: room1,
                server: "localhost:7224",
                owner: name1,
                rights: 0b11111,
                title: "Test Room 1",
                description: "some desc",
                visibility: 0,
                icon: "shopping",
                debug: true,
                confirmed: true
            }
        ]);


    }]
];

export default list;