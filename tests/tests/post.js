import { generateSigningKey, sign } from '../../dist/sys/crypto.js';
import { PERMISSIONS } from '../../dist/server/permissions.js';
import { uts, wait } from '../../dist/sys/tools.js';
import { act_error } from '../../dist/server/errors.js';

let name1 = "testUser1";
let name2 = "testUser2";
let name3 = "testUser3";
let accountKey = "123456789";

let room1 = "r1";
let room2 = "r2";
let room3 = "r3";

let { privateKey, publicKey } = await generateSigningKey();

const list = [
    ["signup", async (req) => {
        await req({}, "signup", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "ok", "");

        await req({}, "signup", {
            name: name1,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.ACCOUNT_EXISTS);

        await req({}, "signup", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "ok", "");

        await req({}, "signup", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.config);
    }], ["remote", async (req) => {
        let signature = (await req({ "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}` },
            "createSignature", {
            publicKey
        }, "ok", null)).sign;
        let falseSignature = await sign("lol", privateKey);
        let token = (await req({}, "requestTempToken", {}, "ok", { token: null })).token;

        await req({
            "authorization": `Bearer ${token}`
        }, "remote1", {
            name: name1,
            server: "localhost:7223",
            publicKey,
            sign: signature
        }, "error", act_error.SIGNATURE);

        await req({
            "authorization": `Bearer ${token}`
        }, "remote1", {
            name: name1,
            server: "localhost:7224",
            publicKey,
            sign: falseSignature
        }, "error", act_error.SIGNATURE);

        let challenge = (await req({
            "authorization": `Bearer ${token}`
        }, "remote1", {
            name: name1,
            server: "localhost:7224",
            publicKey,
            sign: signature
        }, "ok", null)).challenge;

        await req({
            "authorization": `Bearer ${token}`
        }, "remote1", {
            name: name1,
            server: "localhost:7224",
            publicKey,
            sign: signature
        }, "error", act_error.WRONG_STATE);

        await req({
            "authorization": `Bearer ${token}`
        }, "remote2", {
            sign: await sign(challenge + "lol", privateKey)
        }, "error", act_error.SIGNATURE);

        await req({
            "authorization": `Bearer ${token}`
        }, "remote2", {
            sign: await sign(challenge, privateKey)
        }, "ok", "");

    }], ["account", async (req) => {
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "getMyAccount", {}, "ok", {
            rights: PERMISSIONS.ALL,
            name: name1,
            viewable: true,
            maxRooms: 2,
            maxRoomSize: 10,
            maxUsersPerRoom: 2,
        });
        await req({
            "authorization": `Digest name=${name2} server=localhost:7224 accountKey=${accountKey}`
        }, "getMyAccount", {}, "ok", {
            rights: PERMISSIONS.DEFAULT,
            name: name2,
            viewable: true,
            maxRooms: 2,
            maxRoomSize: 10,
            maxUsersPerRoom: 2,
        });
    }], ["change account", async (req) => {
        await req({
            "authorization": `Digest name=${name2} server=localhost:7224 accountKey=${accountKey}`
        }, "changePassword", {
            accountKey: accountKey + "lol"
        }, "ok", "");
        await req({}, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey
        }, "error", act_error.AUTH);
        await req({}, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey: accountKey + "lol"
        }, "ok", "");
        await req({
            "authorization": `Digest name=${name2} server=localhost:7224 accountKey=${accountKey + "lol"}`
        }, "deleteAccount", {}, "ok", "");
        await req({}, "signin", {
            name: name2,
            server: "localhost:7224",
            accountKey: accountKey + "lol"
        }, "error", act_error.AUTH);
    }], ["admin", async (req) => {
        let resp = await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "getAccounts", {}, "ok", [
            {
                accID: 1,
                rights: 65535,
                name: "testUser1",
                viewable: true,
                deleted: false,
                deletedTime: 0,
                maxRooms: 2,
                maxRoomSize: 10,
                maxUsersPerRoom: 2
            }, {
                accID: null,
                rights: 3,
                name: "testUser2",
                viewable: true,
                deleted: true,
                deletedTime: null,
                maxRooms: 2,
                maxRoomSize: 10,
                maxUsersPerRoom: 2
            }
        ]);
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "setPermissions", {
            accID: resp[1].accID,
            rights: 5
        }, "ok", "");
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "getAccounts", {}, "ok", [
            {
                accID: 1,
                rights: 65535,
                name: "testUser1",
                viewable: true,
                deleted: false,
                deletedTime: 0,
                maxRooms: 2,
                maxRoomSize: 10,
                maxUsersPerRoom: 2
            }, {
                accID: 3,
                rights: 5,
                name: "testUser2",
                viewable: true,
                deleted: true,
                deletedTime: null,
                maxRooms: 2,
                maxRoomSize: 10,
                maxUsersPerRoom: 2
            }
        ]);
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "addOTA", {
            token: "12345678",
            name: "tokenname",
            expires: uts() + 1,
            usesLeft: -1
        }, "ok", "");
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "getOTAs", {}, "ok", [
            {
                token: "12345678",
                name: "tokenname",
                expires: null,
                usesLeft: -1
            }
        ]);
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "addOTA", {
            token: "abc",
            name: "tokenname2",
            expires: -1,
            usesLeft: -1
        }, "ok", "");
        await wait(2000);
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "getOTAs", {}, "ok", [{
            token: "abc",
            name: "tokenname2",
            expires: -1,
            usesLeft: -1
        }]);
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "deleteOTA", { token: "abc" }, "ok", "");
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "getOTAs", {}, "ok", []);
    }], ["room Owner", async (req) => {
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "createRoom", {
            room: room1,
            server: "localhost:7224",
            title: "Test Room 1",
            description: "some desc",
            visibility: 0,
            icon: "shopping"
        }, "ok", "");
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "createRoom", {
            room: room1,
            server: "localhost:7224",
            title: "Test Room 1",
            description: "some desc",
            visibility: 0,
            icon: "shopping"
        }, "error", act_error.ROOM_EXISTS);

        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "createRoom", {
            room: room2,
            server: "localhost:7224",
            title: "Test Room 2",
            description: "some desc 2",
            visibility: 1,
            icon: ""
        }, "ok", "");
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "createRoom", {
            room: room3,
            server: "localhost:7224",
            title: "Test Room 2",
            description: "some desc 2",
            visibility: 1,
            icon: ""
        }, "error", act_error.ROOM_LIMIT);

        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "listRooms", {}, "ok", [
            {
                name: room1,
                server: "localhost:7224",
                owner: name1,
                rights: 0b11111,
                title: "Test Room 1",
                description: "some desc",
                visibility: 0,
                icon: "shopping",
                debug: true,
                confirmed: true
            }, {
                name: room2,
                server: "localhost:7224",
                owner: name1,
                rights: 0b11111,
                title: "Test Room 2",
                description: "some desc 2",
                visibility: 1,
                icon: "",
                debug: true,
                confirmed: true
            }
        ]);
        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "deleteRoom", {
            room: room2 + "lol",
            server: "localhost:7224"
        }, "error", act_error.ROOM_NOT_EXISTS);

        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "deleteRoom", {
            room: room2,
            server: "localhost:7224"
        }, "ok", "");

        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "deleteRoom", {
            room: room2,
            server: "localhost:7224"
        }, "error", act_error.ROOM_NOT_EXISTS);

        await req({
            "authorization": `Digest name=${name1} server=localhost:7224 accountKey=${accountKey}`
        }, "listRooms", {}, "ok", [
            {
                name: room1,
                server: "localhost:7224",
                owner: name1,
                rights: 0b11111,
                title: "Test Room 1",
                description: "some desc",
                visibility: 0,
                icon: "shopping",
                debug: true,
                confirmed: true
            }
        ]);
    }]
];

export default list;